% DMRTOOL(1) Version 0.9 | dmml documentation

NAME
====

**dmrtool** -- Manipulate list of methylation reads

SYNOPSIS
========

| **dmrtool** **view** \[**-b**\] { _file_.dmrb | _file_.tsv }

DESCRIPTION
===========

The tool converts sorted list of methylation reads between binary and ascii forms and outputs them in the specified form. The input should be sorted--this is not enforced but the estimator requires sorted data.

Options
-------

-b

:   Specifies that the output is in binary (default: no).

FILE FORMATS
============

The ASCII variant is a tab-separated plain text file with exactly four decimal fields per data row. The header fields must be "#site", "read", "value", and "cookie", in that particular order.

The columns site and read represent 1-based site (genomic location) and read (results linking within a same read) indices. Maximum supported value for each of these fields is 2^32-1. The column value contains the 0-based read state (0 or 1 for methylatoin), with a maximum of 2^64-1, and cookie can contain arbitrary data (not used by the estimator) fitting in the upper bits of value (a 1-bit value allows a 63-bit cookie). The main purpose is padding, but it can be used e.g. for data to map the site indices back to genomic coordinates. The cookie can be zero. 

The binary variant has an four 32-bit word header, 128-bit limit record, and an arbitrary number of 128-bit records. The first word of the header is the magic number 'brmd' in ASCII in native byte order, and the next three are reserved (set to zero). Each record has a 32-bit site and read fields, and a 64-bit value/cookie field. For the limit record, the site, read, and value/cookie fields contain one plus the maximum site, read, and value, respectively, in native byte order. For each record in the payload, the site and read fields contain the 0-based site and read indices, respectively, and the value/cookie field contains the value and the cookie in the low and high order bits, respectively.

For the estimator the records must be grouped (not necessarily sorted) by the read index and then sorted by the site index. Intermediate files need not to obey this, but it is highly recommended.

The description above might lack technical detail. Please refer to the source code for more details.

BUGS
====

Inputs values are not checked for overflow.
Values/cookies might be silently truncated to 32-bits for some platforms.

SEE ALSO
========

dmml

AUTHOR
======

Antti Hakkinen.
