
# This file is subject to Mozilla Public License
# Copyright (c) 2016 Antti Hakkinen

CC = cc
AR = ar
RM = rm -f

CFLAGS = -DNDEBUG -O3 -fomit-frame-pointer -fPIC $(CFLAGS.EXTRA)
LIBS = -lm $(LIBS.EXTRA)

dmml_objects = src/dmml.o src/dmrb.o src/tablewriter.o
dmrtool_objects = src/dmrtool.o src/dmrb.o
pd2asc_objects = src/pd2asc.o
lib_objects = src/chi2cdf.o src/halton.o src/imath.o src/model.o src/parser.o src/solver.o

objects = $(dmml_objects) $(dmrtool_objects) $(lib_objects) $(pd2asc_objects)
targets = dmml dmrtool libdmml.a libdmml.so pd2asc

all: $(targets)

cleanobj:
	$(RM) $(objects)

clean: cleanobj
	$(RM) $(targets)

.c.o: %.c %.h
	$(CC) $(CFLAGS) -c -o "$@" "$<"

dmml: $(dmml_objects) libdmml.a
	$(CC) $(CFLAGS) -o "$@" $(dmml_objects) libdmml.a $(LIBS)

dmrtool: $(dmrtool_objects)
	$(CC) $(CFLAGS) -o "$@" $(dmrtool_objects) $(LIBS)

libdmml.a: $(lib_objects)
	$(AR) -r -cu "$@" $(lib_objects)

libdmml.so: $(lib_objects)
	$(CC) $(CFLAGS) -shared -o "$@" $(lib_objects) $(LIBS)

pd2asc: $(pd2asc_objects)
	$(CC) $(CFLAGS) -o "$@" $(pd2asc_objects) $(LIBS)

man:
	pandoc --standalone --to man dmml.1.md -o dmml.1
	pandoc --standalone --to man dmrtool.1.md -o dmrtool.1
	pandoc --standalone --to man pd2asc.1.md -o pd2asc.1

.PHONY: all cleanobj clean man
.SUFFIXES: .c.o
