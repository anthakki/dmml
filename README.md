
dmml
====

`dmml` is a tool for identifying differentially methylated sites in tumor samples with varying, unknown tumor cell fraction using a maximum-likelihood method. For the details about the method and validation, please refer to our [publication on the matter][1].

Major features:

- Comparison of up to 31 tumor samples simultaneously
- Zero, one, or multiple control samples
- Tumor purity of each sample is automatically estimated; alternatively, prior knowledge about the tumor cell fraction can be injected
- The estimator models co-methylation of neighboring sites locally in each neighborhood, which properly accounts for correlations within and across CG islands

[1]: https://doi.org/10.1093/bioinformatics/bty310

Installation
------------

To build the program, you will need a C90 compiler, `stdint.h` (comes with C99, POSIX, and most modern compilers), and a POSIX make. The development occurs using GCC on x86-64 Linux, but there is no reason for the code not to work even on non-Intel and Windows platforms. Issue `make` to build the tool and the C libraries, and `make -C matlab` to build the MATLAB interfaces. MATLAB is not required--the C driver program can be used to run typical analyses, as described below.

The tools are statically linked to `libdmml`, so the executables are stand-alone and can be installed in an arbitrary location. 

The tools can be built with optional `zlib` support, which enables transparent gzip-compression of both the inputs and outputs. For this, define the macro `HAVE_ZLIB` in your C compiler and link the program with zlib, e.g. by issuing:

```
	make CFLAGS.EXTRA=-DHAVE_ZLIB LIBS.EXTRA=-lz
```

The program is also available as an [Anduril component](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=sequencing.DMML), which facilitates integration with other tools.

Overview and usage
------------------

A typical workflow is as follows:

- Prepare input files in the sorted binary format the estimator expects. The file format is described in detail in the manual page of [**dmrtool**](dmrtool.1.md). In particular, to exploit the co-methylation modeling, the read IDs linking the observed methylation states must be recorded.
- Run the estimator. This and the parameters are described in the manual page of [**dmml**](dmml.1.md).
- Post-process the output. The tool [**pd2asc**](pd2asc.1.md) can be useful if you prefer to save the estimator results in the binary format (e.g. **--output-pvalues**=b:_fn_).

The input file for each tumor sample are expected to contain the methylation read states (methylated/unmethylated) from the sequencing data grouped by the read ID and sorted by the genomic location. Typically, these data must be extracted directly from the [BAM files](https://dx.doi.org/10.1093%2Fbioinformatics%2Fbtp352) as no standard intermediate format exists for such information. In addition, the estimator does not use genomic coordinates but contiguous site indices (a running number to identify the genomic location) so an index should be build in this process, such that the results can be mapped back to the genome. Sequence trimming, alignment and quality control should be done prior to running the estimator and is out of scope of this document.

We have prepared examples in `test/example-tumor-1.tsv`, `test/example-tumor-2.tsv`, and `test/example-control.tsv`. These files were generated using a Monte Carlo method from the human reference methylome rather than from measurement data but the format is similar to how we handle our data. The site indices are 1-based foreign keys into `test/example-sites.tsv`, which contains the original genomic locations, and the read indices are 1-based foreign keys into `test/example-reads.tsv`, which contains the original read IDs (these tables are not used by the estimator and are included for illustrative purposes). A value of 1 indicates a methylated read and a value of 0 an unmethylated one. The cookie contains an opaque user-defined value, and is set to zero in our examples.

Alternatively, sitewise counts of methylated and unmethylated reads can be transformed into such format, but in this case co-methylation modeling has no information to work on as the depence information between the samples within a single read is lost. The two forms of data can be mixed.

Once the files have been prepared, the next step is to convert them into the binary form using [**dmrtool**](dmrtool.1.md) and run the estimator [**dmml**](dmml.1.md). The following performs a pairwise comparison of two tumor samples without a control (the examples assume a POSIX Bourne shell):

```
	dmrtool view -b test/example-tumor-1.tsv >t1.dmrb
	dmrtool view -b test/example-tumor-2.tsv >t2.dmrb

	dmml t1.dmrb t2.dmrb --output-params=t12-nc-params.tsv \
		--output-probs=t12-nc-probs.tsv --output-pvalues=t12-nc-pvalues.tsv
```

and the following adds a comparison with a matched control whose results are written in a different set of output files:

```
	dmrtool view -b test/example-control.tsv >c.dmrb

	dmml t1.dmrb t2.dmrb c.dmrb --output-params=t12-mc-params.tsv \
		--output-probs=t12-mc-probs.tsv --output-pvalues=t12-mc-pvalues.tsv
```

Further options can be provided to [**dmml**](dmml.1.md) to add prior information or control the optimization process.

The following prints the estimated tumor purities and the error parameter for each comparison:

```
	head -v t12-nc-params.tsv t12-mc-params.tsv
```

The following MATLAB code adjusts for multiple hypothesis testing and prints the sites where the methylation is expected to differ at a significance level 0.05 in the two tumor samples for the no-control comparison:

```
	matlab -nodesktop -nojvm <<-EOT
		% read p-values
		pv= dlmread('t12-nc-pvalues.tsv', '\t', 1, 0);
		% get site names
		s= strsplit(fileread('test/example-sites.tsv'), '\n'); s= s(2:end-1);
		% adjust p-values and print significant
		fprintf(1, '%s\n', s{mafdr(pv(:, end), 'BHFDR', true) < 0.05});
		% done
		quit;
	EOT
```

Here is the corresponding R code:

```
	R --vanilla -q <<-EOT
		# read p-values
		pv <- read.table('t12-nc-pvalues.tsv', header=T, sep = "\t");
		# get site names
		s <- read.table('test/example-sites.tsv', header=F, stringsAsFactors=F);
		# adjust p-values and print significant
		print(s[p.adjust(pv[['p.value.left.vs.right']], method='BH') < 0.05, ]);
	EOT
```

The following prints the maximum posterior estimates of the purified methylation patterns of the each type of cancer cells in the two tumor samples:

```
	matlab -nodesktop -nojvm <<-EOT
		% read state probabilities
		pr= dlmread('t12-nc-probs.tsv', '\t', 1, 0);
		% get site names
		s= strsplit(fileread('test/example-sites.tsv'), '\n'); s= s(2:end-1);
		% get masks
		m= fix(mod( (( (1:2^3).'-1 )) * (( 2.^(-(1:2)) )), 2 ));
		% collect probabilitiese & print estimates
		args= [ s(:).'; num2cell( pr * m > .5 ).' ];
		fprintf(1, '%s\t%d\t%d\n', args{:});
		% done
		quit;
	EOT
```

Here is the corresponding R code:

```
	R --vanilla -q <<-EOT
		# read state probabilities
		pr <- as.matrix(read.table('t12-nc-probs.tsv', header=T));
		# get site names
		s <- read.table('test/example-sites.tsv', header=F, stringsAsFactors=F);
		# get masks
		m <- trunc( outer( (1:2^3)-1, 2^-(1:2) ) %% 2 )
		# collect probabilitiese & print estimates
		print(cbind( s, ( pr %*% m > .5 ) + 0. ))
	EOT
```

The format of the output files is further discussed in the manual page of [**dmml**](dmml.1.md).

More examples can be found in the [**dmml**](dmml.1.md) manual page and more complex settings can be handle using the C or the MATLAB API: for these please refer to the header files in `src/dmml/*.h` and the MATLAB documentation in `matlab/*.m`, especially the sample driver script `matlab/dmml_test.m`.

Copying
-------

All files are subject to Mozilla Public License v. 1.1. See `LICENSE.txt` for details.
Copyright (c) 2016 Antti Hakkinen.
