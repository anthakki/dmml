
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <mex.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isUint32(const mxArray *arg)
{ return mxGetClassID(arg) == mxUINT32_CLASS; }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

#define MEXFILENAME "dmml_expect_update"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const double *latent;
	mwSize cols, rows, inputs, modellen, i;
	const uint32_T *inputtab, *counts;
	double *expect;

	/* Check input count */
	if (nrhs < 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check inputs */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isMatrix(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":latent", "The input LATENT must be a full real double matrix.");
	if (!(isFullReal(prhs[1]) && isUint32(prhs[1]) && isMatrix(prhs[1]) && mxGetN(prhs[1]) == mxGetM(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":inputtab", "The input INPUTTAB must be an uint32 matrix.");
	if (!(isFullReal(prhs[2]) && isUint32(prhs[2]) && isMatrix(prhs[2]) && mxGetM(prhs[2]) == mxGetM(prhs[1]) && mxGetN(prhs[2]) == mxGetN(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":counts", "The input COUNTS must be a compatible uint32 matrix."); 

	/* Get dimensions */
	latent = mxGetPr(prhs[0]);
	cols = mxGetM(prhs[0]);
	rows = mxGetN(prhs[0]);
	inputtab = (uint32_T *)mxGetData(prhs[1]);
	inputs = mxGetM(prhs[1]);
	counts = (uint32_T *)mxGetData(prhs[2]);

	/* Check input table for output dimension */
	modellen = 1;
	for (i = 0; i < inputs * cols; ++i)
		if (!(inputtab[i] < modellen))
			modellen = inputtab[i] + 1;

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(modellen, 1, mxREAL);
	expect = mxGetPr(plhs[0]);

	/* Populate output */
	/* NOTE: mxCreateDoubleMatrix() zeroes expect */
	dmml_expect_update(expect, rows, cols, inputs, latent, inputtab, counts);
}
