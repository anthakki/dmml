
% DMML_CHI2CDF Cumulative distribution for chi-squared distribution
%   y= DMML_CHI2CDF(x, nu) evaluates the cumulative density of a chi-squared
%   distribution with nu degrees of freedom at x. DMML_CHI2CDF(x, nu, 'upper')
%   computes the probability mass on the upper tail.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
