
% DMML_EXPECT_UPDATE Accumulate expected model likelihood
%   expect= DMML_EXPECT_UPDATE(latent, inputtab, counts) computes the update
%   for the expected model likelihood.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
