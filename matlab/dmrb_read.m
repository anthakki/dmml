
% DMRB_READ Read a DMRB file
%   data= DMRB_READ(filename) reads a DMRB file. The output is an N-by-3
%   array where the first column represents site indices, the second read
%   indices, and the third read values (0 or 1).

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
