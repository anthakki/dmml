
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <mex.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
checkCount(mwSize *count, double value)
{
	*count = (mwSize)value;
	return *count >= 0 && *count == value;
}

#define MEXFILENAME "dmml_input_table"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize letters, order, tracks, trackmask;
	mwSize cols, inputs;
	uint32_T *table;

	/* Check inputs */
	if (nrhs != 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Wrong number of inputs");

	/* Suppress used uninitialized warnings */
	letters=0; order=0; tracks=0; trackmask=0;

	/* Get dimensions */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && mxGetNumberOfElements(prhs[0]) == 1 && checkCount(&letters, *mxGetPr(prhs[0]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":letters", "The input LETTERS must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && mxGetNumberOfElements(prhs[1]) == 1 && checkCount(&order, *mxGetPr(prhs[1]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":order", "The input ORDER must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[2]) && mxIsDouble(prhs[2]) && mxGetNumberOfElements(prhs[2]) == 1 && checkCount(&tracks, *mxGetPr(prhs[2]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":tracks", "The input TRACKS must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[3]) && mxIsDouble(prhs[3]) && mxGetNumberOfElements(prhs[3]) == 1 && checkCount(&trackmask, *mxGetPr(prhs[3]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":trackmask", "The input TRACKMASK must be a full real scalar double representing a non-negative integer.");

	/* Get table dimensions */ 
	cols = dmml_latent_cols(letters, order, tracks);
	inputs = dmml_input_size(letters, order);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateNumericMatrix(inputs, cols, mxUINT32_CLASS, mxREAL);
	table = (uint32_T *)mxGetData(plhs[0]);

	/* Populate table */
	dmml_input_table(table, letters, order, tracks, trackmask);
}
