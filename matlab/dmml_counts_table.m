
% DMML_COUNTS_TABLE Count input data
%   counts= DMML_COUNTS_TABLE(sites, letters, order, data) counts the inputs in
%   data. Here, data is N-by-3 matrix where the first column represents site
%   indices, the second read indices, and the third is the read value (0 or 1).

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
