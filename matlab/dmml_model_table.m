
% DMML_MODEL_TABLE Compute model probability lookup table
%   modeltab= DMML_MODEL_TABLE(order, trackmask, alphas, err) computes the
%   model probability lookup table.  One such table is neede for each input
%   type. Here, order is the correlation order, and trackmask is the bitmap
%   representing which tracks are used, alphas the mixing parameters, and err
%   the error parameter for the input type.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
