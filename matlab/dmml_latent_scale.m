
% DMML_LATENT_SCALE Scale the distribution of latent states
%   [latent, scale]= DMML_LATENT_SCALE(latent) scales the distribution of
%   latent states such the distributions sum to unity. Returns also the scale,
%   which is related to the likelihood.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
