
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <dmml/parser.h>
#include <mex.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

static
int
checkCount(mwSize *count, double value)
{
	*count = (mwSize)value;
	return *count >= 0 && *count == value;
}

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

#define MEXFILENAME "dmml_counts_table"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize sites, letters, order, samples, inputs, rows, i, site, read, letter;
	const double *data;
	uint32_T *counts, last_read;
	dmml_parser_t parser;

	/* Check inputs */
	if (nrhs < 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Set defaults to suppress warnings */
	sites = 0; letters = 0; order = 0;

	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isScalar(prhs[0]) && checkCount(&sites, *mxGetPr(prhs[0]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":sites", "The input SITES is must be a full real double representing a non-negative integer.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && isScalar(prhs[1]) && checkCount(&letters, *mxGetPr(prhs[1]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":letters", "The input LETTERS is must be a full real double representing a non-negative integer.");
	if (!(isFullReal(prhs[2]) && mxIsDouble(prhs[2]) && isScalar(prhs[2]) && checkCount(&order, *mxGetPr(prhs[2]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":order", "the input ORDER is must be a full real double representing a non-negative integer.");
	if (!(isFullReal(prhs[3]) && mxIsDouble(prhs[3]) && isMatrix(prhs[3]) && mxGetN(prhs[3]) == 3))
		mexErrMsgIdAndTxt(MEXFILENAME ":data", " The input DATA must be a full real N-by-3 double matrix.");

	/* Get data */
	data = mxGetPr(prhs[3]);
	samples = mxGetM(prhs[3]);

	/* Compute dimensions */
	inputs = dmml_input_size(letters, order);
	rows = dmml_latent_rows(sites, order);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateNumericMatrix(inputs, rows, mxUINT32_CLASS, mxREAL);
	counts = (uint32_T *)mxGetData(plhs[0]);

	/* Create the parser */
	dmml_parser_init(&parser, letters, order);

	/* Suppress more warnings */
	site = 0; read = 0; letter = 0;

	/* Loop */
	last_read = (uint32_t)-1;
	for (i = 0; i < samples; ++i)
	{
		/* Parse values */
		if (!(checkCount(&site, data[i])  && checkCount(&read, data[i+1*samples]) &&
				checkCount(&letter, data[i+2*samples])))
			mexErrMsgIdAndTxt(MEXFILENAME ":data", "The input DATA contains invalid values.");

		/* Check that sites are in bounds */
		if (!(1 <= site && site <= sites))
			mexErrMsgIdAndTxt(MEXFILENAME ":data", "The input DATA contains a site that is zero or too large.");
		if (!(letter < letters))
			mexErrMsgIdAndTxt(MEXFILENAME ":data", "The input DATA contains an invalid letter.");

		/* Feed data to the parser */
		if (dmml_parser_push(&parser, counts, 0, sites, read != last_read, site - 1, letter) != 0)
			mexErrMsgIdAndTxt(MEXFILENAME ":data", "The input (%g, %g, %g) is not sorted within the group.", (double)site, (double)read, (double)letter);

		/* Moving on.. */
		last_read = read;
	}

	/* Finish parsing */
	dmml_parser_flush(&parser, counts, 0, sites);
}
