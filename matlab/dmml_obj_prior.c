
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/solver.h>
#include <mex.h>
#include <string.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isVector(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetM(arg) ||
		mxGetNumberOfElements(arg) == mxGetN(arg); }

static
int
hasEqualSize(const mxArray *arg1, const mxArray *arg2)
{
	mwSize dims;
	const mwSize *size;

	/* Check dimension */
	dims = mxGetNumberOfDimensions(arg1);
	if (mxGetNumberOfDimensions(arg2) != dims)
		return 0;

	/* Check size */
	size = mxGetDimensions(arg1);
	if (memcmp(mxGetDimensions(arg2), size, dims * sizeof(*size)) != 0)
		return 0;

	return 1;
}

#define MEXFILENAME "dmml_obj_prior"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const double *parms, *means, *precs;
	mwSize dim, i;
	double *obj;

	/* Check input count */
	if (nrhs < 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check inputs */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isVector(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":parms", "The input PARMS must be a full real double vector.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && hasEqualSize(prhs[1], prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":means", "The input MEANS must be a compatible full real double vector.");
	if (!(isFullReal(prhs[2]) && mxIsDouble(prhs[2]) && hasEqualSize(prhs[2], prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":precs", "The input PRECS must be a compatible full real double vector.");

	/* Get dimension */
	parms = mxGetPr(prhs[0]);
	dim = mxGetNumberOfElements(prhs[0]);
	means = mxGetPr(prhs[1]);
	precs = mxGetPr(prhs[2]);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(dmml_obj_size(dim), 1, mxREAL);
	obj = mxGetPr(plhs[0]);
	/* NOTE: mxCreateDoubleMatrix() zeroes obj */

	/* Compute */
	for (i = 0; i < dim; ++i)
		dmml_obj_prior(obj, dim, i, parms[i], means[i], precs[i]);
}
