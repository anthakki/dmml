
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/solver.h>
#include <mex.h>
#include <string.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isVector(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetM(arg) ||
		mxGetNumberOfElements(arg) == mxGetN(arg); }

#define MEXFILENAME "dmml_obj_solve"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const double *oldparms, *obj;
	mwSize dim, objlen;
	double *parms;

	/* Check input count */
	if (nrhs < 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check parameters */
	if (!(isFullReal(prhs[0]) && isVector(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":parms", "The input PARMS must be a full real double vector.");

	/* Get dimension */
	oldparms = mxGetPr(prhs[0]);
	dim = mxGetNumberOfElements(prhs[0]);
	objlen = dmml_obj_size(dim);

	/* Check objective */
	if (!(isFullReal(prhs[1]) && isVector(prhs[1]) && mxGetM(prhs[1]) == objlen))
		mexErrMsgIdAndTxt(MEXFILENAME ":obj", "The input OBJ must be a compatible full real double vector.");

	/* Get pointer */
	obj = mxGetPr(prhs[1]);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateNumericArray(mxGetNumberOfDimensions(prhs[0]), mxGetDimensions(prhs[0]), mxDOUBLE_CLASS, mxREAL);
	parms = mxGetPr(plhs[0]);
	memcpy(parms, oldparms, dim * sizeof(*oldparms));

	/* Solve */
	dmml_obj_solve(parms, obj, dim);
}
