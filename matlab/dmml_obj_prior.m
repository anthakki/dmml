
% DMML_OBJ_PRIOR Add prior information to objective
%   obj= DMML_OBJ_PRIOR(parms, means, precs) ands prior information about the
%   parameters. Here, parms are the current parameter values, and means and
%   precs the means and precision of the prior.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
