
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <mex.h>

/* Computes number of set bits in x */
extern int popcntl(unsigned long x);

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

static
int
checkCount(mwSize *count, double value)
{
	*count = (mwSize)value;
	return *count >= 0 && *count == value;
}

#define MEXFILENAME "dmml_model_table"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize order, trackmask, trackcnt;
	const double *alphas;
	double err, *modeltab;

	/* Check input count */
	if (nrhs < 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments");
	if (nrhs > 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments");

	/* Set defaults to suppress warnings */
	order = 0; trackmask = 0;

	/* Parse dimension arguments */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isScalar(prhs[0]) && checkCount(&order, *mxGetPr(prhs[0]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":order", "The input ORDER must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && isScalar(prhs[0]) && checkCount(&trackmask, *mxGetPr(prhs[1]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":trackmask", "The input TRACKMASK must be a full real scalar double representing a non-negative integer.");

	/* Get track count */
	trackcnt = popcntl(trackmask);

	if (!(isFullReal(prhs[2]) && mxIsDouble(prhs[2]) && mxGetNumberOfElements(prhs[2]) == trackcnt))
		mexErrMsgIdAndTxt(MEXFILENAME ":alphas", "The input ALPHAS must be a %lu element full double vector.", (unsigned long)trackcnt);
	if (!(isFullReal(prhs[3]) && mxIsDouble(prhs[3]) && isScalar(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":err", "The input ERR must be a scalar double.");

	/* */
	alphas = mxGetPr(prhs[2]);
	err = *mxGetPr(prhs[3]);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(dmml_model_size(order, trackmask), 1, mxREAL);
	modeltab = mxGetPr(plhs[0]);

	/* Populate table */
	/* NOTE: mxCreateDoubleMatrix() zeroes modeltab */
	dmml_model_table(modeltab, order, trackmask, alphas, err);
}
