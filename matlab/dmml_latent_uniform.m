
% DMML_LATENT_UNIFORM Create an uniform latent distribution 
%   latent= DMML_LATENT_UNIFORM(sites, letters, order, tracks) creates an
%   uniform latent distribution, which is useful for initialization. 

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
