
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <mex.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

static
int
checkCount(mwSize *count, double value)
{
	*count = (mwSize)value;
	return *count >= 0 && *count == value;
}

#define MEXFILENAME "dmml_latent_uniform"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize sites, letters, order, tracks, cols, rows;
	double *latent;

	/* Check input counts */
	if (nrhs < 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Suppress warnings about unused variables */
	sites = 0; letters = 0; order = 0; tracks = 0;

	/* Check arguments */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isScalar(prhs[0]) && checkCount(&sites, *mxGetPr(prhs[0]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":sites", "The input SITES must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && isScalar(prhs[1]) && checkCount(&letters, *mxGetPr(prhs[1]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":letters", "The input LETTERS must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[2]) && mxIsDouble(prhs[2]) && isScalar(prhs[2]) && checkCount(&order, *mxGetPr(prhs[2]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":order", "The input ORDER must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[3]) && mxIsDouble(prhs[3]) && isScalar(prhs[3]) && checkCount(&tracks, *mxGetPr(prhs[3]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":tracks", "The input TRACKS must be a full real scalar double representing a non-negative integer.");

	/* Compute dimensions */
	cols = dmml_latent_cols(letters, order, tracks);
	rows = dmml_latent_rows(sites, order);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(cols, rows, mxREAL);
	latent = mxGetPr(plhs[0]);

	/* Scale */
	dmml_latent_scale(latent, cols, rows);
}
