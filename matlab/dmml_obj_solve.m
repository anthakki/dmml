
% DMML_OBJ_SOLVE Optimize objective
%   parms= DMML_OBJ_SOLVE(parms, obj) finds the parameters that optimize the
%   objective

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
