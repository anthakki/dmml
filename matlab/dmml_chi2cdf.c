
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/chi2cdf.h>
#include <mex.h>

static
int
isFullRealDouble(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsDouble(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; }

static
int
checkSize(mwSize *size, double value)
{
	/* Cast & compare */
	*size = (mwSize)value;
	return *size >= 0 && *size == value;
}

static
int
isString(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsChar(arg); }

static
int
compareString(const mxArray *arg, const char *string)
{
	const mxChar *data;
	mwSize size, i;
	
	/* Get characters */
	data = mxGetChars(arg);
	size = mxGetNumberOfElements(arg);

	/* Compare */
	for (i = 0; i < size; ++i)
		if (string[i] == '\0' || data[i] != string[i])
			return (int)string[i] - (int)data[i];

	return (int)string[i];
}

#define MEXFILENAME "dmml_chi2cdf"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	int tail;
	mwSize nu, length, i;
	const double *xvals;
	double *yvals;

	/* Defaults */
	tail = -1;

	/* Check input arguments */
	if (nrhs < 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check inputs */
	if (!(isFullRealDouble(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":x", "The input X must be a full real double array.");
	if (!(isFullRealDouble(prhs[1]) && isScalar(prhs[1]) && checkSize(&nu, *mxGetPr(prhs[1]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":nu", "The input NU must be a compatible full real double vector.");

	/* Check tail argument */
	if (nrhs >= 3 && !mxIsEmpty(prhs[2]))
	{
		/* Check */
		if (!(isString(prhs[2]) && compareString(prhs[2], "upper") == 0))
			mexErrMsgIdAndTxt(MEXFILENAME ":tail", "The input TAIL must be 'upper'.");

		/* Use upper tail */
		tail = 1;
	}

	/* GEt data */
	xvals = mxGetPr(prhs[0]);
	length = mxGetNumberOfElements(prhs[0]);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateNumericArray(mxGetNumberOfDimensions(prhs[0]), mxGetDimensions(prhs[0]), mxDOUBLE_CLASS, mxREAL);
	yvals = mxGetPr(plhs[0]);

	/* Compute */
	for (i = 0; i < length; ++i)
		yvals[i] = dmml_chi2cdf(xvals[i], nu, tail);
}
