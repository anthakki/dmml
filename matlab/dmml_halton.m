
% DMML_HALTON Halton's sequnce
%   x= DMML_HALTON(n, c) generates the n-point Halton's sequence in [0,1]^d
%   starting at c, where the dimension is d= length(c). In the output x,
%   columns correspond to points and rows variables.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
