
% DMML_MARGINAL_TABLE Compute marginal distribution of the joint methylation patterns
%   y= DMML_MARGINAL_TABLE(letters, tracks, latent) computes the distribution
%   of the joint methylation patterns. 

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
