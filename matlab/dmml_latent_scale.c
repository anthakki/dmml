
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <mex.h>
#include <string.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

#define MEXFILENAME "dmml_latent_scale"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize cols, rows;
	const double *oldlatent;
	double *latent, scale;

	/* Check input counts */
	if (nrhs < 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check inputs */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isMatrix(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":latent", "The input LATENT must be a full real double matrix.");

	/* Get data */
	oldlatent = mxGetPr(prhs[0]);
	cols = mxGetM(prhs[0]);
	rows = mxGetN(prhs[0]);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(cols, rows, mxREAL);
	latent = mxGetPr(plhs[0]);
	memcpy(latent, oldlatent, cols * rows * sizeof(*latent));

	/* Scale */
	if (cols > 0)
		scale = dmml_latent_scale(latent, rows, cols);
	else
		scale = 0.;

	/* Store scale */
	plhs[1] = mxCreateDoubleScalar(scale);
}
