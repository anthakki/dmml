
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <mex.h>

static
int
solveOrder(mwSize *order, mwSize cols, mwSize letters, mwSize tracks)
{
	unsigned long base, len;
	
	/* Get base */
	base = 1;
	while (tracks-- > 0)
		base *= letters;

	/* Find order */
	len = base;
	for (*order = 1; len < cols; ++*order)
	{
		/* Multiply, check overflow */
		if ((unsigned long)-1 / len < base)
			return 0;
		len *=  base;
	}

	return len == cols;
}

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

static
int
checkCount(mwSize *count, double value)
{
	*count = (mwSize)value;
	return *count >= 0 && *count == value;
}

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

#define MEXFILENAME "dmml_marginal_table"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize letters, tracks;
	const double *latent;
	mwSize cols, rows, order, sites, marginal_cols, i, j;
	double *marginal;

	/* Check inputs */
	if (nrhs < 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Suppress warnings */
	letters = 0; tracks = 0;

	/* Check inputs */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isScalar(prhs[0]) && checkCount(&letters, *mxGetPr(prhs[0]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":order", "The input LETTERS must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && isScalar(prhs[1]) && checkCount(&tracks, *mxGetPr(prhs[1]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":order", "The input TRACKS must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[2]) && mxIsDouble(prhs[2]) && isMatrix(prhs[2])))
		mexErrMsgIdAndTxt(MEXFILENAME ":latent", "The input LATENT must be a full real double matrix.");

	/* Get dimensions */
	latent = mxGetPr(prhs[2]);
	cols = mxGetM(prhs[2]);
	rows = mxGetN(prhs[2]);

	/* Solve remaining parameters */ 
	if (!solveOrder(&order, cols, letters, tracks))
		mexErrMsgIdAndTxt(MEXFILENAME ":latent", "The input LATENT is of invalid dimensions.");
	if (rows < (order-1))
		mexErrMsgIdAndTxt(MEXFILENAME ":latent", "The input LATENT is of invalid dimensions.");
	sites = rows - (order-1);
	marginal_cols = dmml_marginal_cols(letters, tracks);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(marginal_cols, sites, mxREAL);
	marginal = mxGetPr(plhs[0]);

	/* Populate output */
	for (i = 0; i < sites; ++i)
		for (j = 0; j < marginal_cols; ++j)
			marginal[i*marginal_cols + j] = dmml_marginal_element(letters, order, tracks, latent, i, j);
}
