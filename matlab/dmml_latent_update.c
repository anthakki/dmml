
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/model.h>
#include <mex.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isUint32(const mxArray *arg)
{ return mxGetClassID(arg) == mxUINT32_CLASS; }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

static
int
isVector(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetM(arg) ||
		mxGetNumberOfElements(arg) == mxGetN(arg); }

#define MEXFILENAME "dmml_latent_update"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const uint32_T *inputtab, *counts;
	mwSize inputs, cols, modellen, rows, i;
	const double *modeltab;
	double *latent;

	/* Check inputs */
	if (nrhs < 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check inputs */
	if (!(isFullReal(prhs[0]) && isUint32(prhs[0]) && isMatrix(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":inputtab", "The input INPUTTAB must be an uint32 matrix.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && isVector(prhs[1])))
		mexErrMsgIdAndTxt(MEXFILENAME ":modeltab", "The input MODELTAB must be a full real double vector.");
	if (!(isFullReal(prhs[2]) && isUint32(prhs[2]) && isMatrix(prhs[2]) && mxGetM(prhs[2]) == mxGetM(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":counts", "The input COUNTS must be a compatible uint32 matrix."); 

	/* Check dimensions */
	inputtab = (uint32_T *)mxGetData(prhs[0]);
	inputs = mxGetM(prhs[0]);
	cols = mxGetN(prhs[0]);
	modeltab = mxGetPr(prhs[1]);
	modellen = mxGetNumberOfElements(prhs[1]);
	counts = (uint32_T *)mxGetData(prhs[2]);
	rows = mxGetN(prhs[2]);

	/* Check input table for overflow */
	for (i = 0; i < inputs * cols; ++i)
		if (!(inputtab[i] < modellen))
			mexErrMsgIdAndTxt(MEXFILENAME ":inputtab", "The input INPUTTAB contains invalid values.");

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(cols, rows, mxREAL);
	latent = mxGetPr(plhs[0]);

	/* Compute */
	/* NOTE: mxCreateDoubleMatrix() zeros memory */
	dmml_latent_update(latent, rows, cols, inputs, inputtab, modeltab, counts);
}
