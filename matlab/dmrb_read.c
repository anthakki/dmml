
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmrb.h> 
#include <mex.h>
#include <string.h>

static
void
expandArray(double *data, mwSize stride, mwSize gap, mwSize count)
{
	mwSize i;

	/* Expands an array of size stride*count to size (stride+gap)*count
		such that it consist of count stride-length runs of data separated by
		gap-length gaps. */
	for (i = count; i-- > 0;)
		memmove(&data[i*(stride + gap)], &data[i*stride], stride * sizeof(*data));
}

static
void
compactArray(double *data, mwSize stride, mwSize gap, mwSize count)
{
	mwSize i;

	/* Undoes the work of expandArray() */
	for (i = 0; i < count; ++i)
		memmove(&data[i*stride], &data[i*(stride+gap)], stride * sizeof(*data));
}

struct Data {
	double *data;
	mwSize ld, size;
};

static
void
initData(struct Data *self)
{
	/* Set up an empty buffer */
	self->data = NULL;
	self->ld = 0;
	self->size = 0;
}

static
double *
pushData(struct Data *self)
{
	mwSize newld;
	double *data;

	/* Out of space? */
	if (!(self->size < self->ld))
	{
		/* Bump size */
		newld = self->ld + self->ld / 2;
		if (newld < 4)
			newld = 4; 

		/* Extend memory */
		data = (double *)mxRealloc(self->data, 3 * newld * sizeof(*data));
		if (data == NULL)
			return NULL;

		/* Reshape data */
		expandArray(data, self->size, newld - self->size, 3);

		/* Update block reference */
		self->data = data;
		self->ld = newld;
	}

	/* Allocate slot */
	return &self->data[self->size++];
}

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isColumn(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetN(arg); } 

#define MEXFILENAME "dmrb_read"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const char *filename;
	struct Data data;
	dmrb_reader_t reader;
	uint64_t mask;
	dmrb_record_t record;
	double *row;

	/* Check input arguments */
	if (nrhs < 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check input */
	if (!(isFullReal(prhs[0]) && mxIsChar(prhs[0]) && isColumn(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":filename", "The input FILENAME must be a string.");

	/* Get filename */
	filename = mxArrayToString(prhs[0]);

	/* Create a new array */
	initData(&data);

	/* Open filename */
	if (dmrb_reader_init(&reader, filename) != 0)
		mexErrMsgIdAndTxt(MEXFILENAME ":open", "Failed to open '%s' as a DMRB file.", filename);

	/* Get mask */
	mask = dmrb_reader_limits(&reader)->value - 1;

	/* Process */
	while (dmrb_reader_read(&reader, &record, 1) > 0)
	{
		/* Allocate a slot */
		row = pushData(&data);
		if (row == NULL)
		{
			dmrb_reader_deinit(&reader);
			mexErrMsgIdAndTxt(MEXFILENAME ":mem", "Out of memory!");
		}

		/* Store record */
		row[0*data.ld] = record.site + 1; /* NOTE: adjust for MATLAB indices */
		row[1*data.ld] = record.read + 1;
		row[2*data.ld] = record.value & mask;
	}

	/* Check for errors */
	if (!(dmrb_reader_eof(&reader) > 0))
	{
		dmrb_reader_deinit(&reader);
		mexErrMsgIdAndTxt(MEXFILENAME ":", "Read error on '%s'.", filename);
	}

	/* Close */
	dmrb_reader_deinit(&reader);

	/* Compact data */
	compactArray(data.data, data.size, data.ld - data.size, 3);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(0, 3, mxREAL);

	/* Swap in the data */
	mxSetPr(plhs[0], data.data);
	mxSetM(plhs[0], data.size);
}
