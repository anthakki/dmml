
% DMML_INPUT_TABLE Compute input translation table
%   inputtab= DMML_INPUT_TABLE(letters, order, tracks, trackmask) computes the
%   lookup table that is needed for input translation. One such table is needed
%   for each input type. Here, letters is the alphabet size, tracks is the
%   number of tracks, and trackmask is the mask of tracks used in this input.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
