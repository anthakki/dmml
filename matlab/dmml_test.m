
% DMML_TEST Test differential methylation
%   [pvals, stats]= DMML_TEST({left, right}, {control}, 'option', value,
%   ...) tests for differential methylation between the samples left and
%   right right, using the optional control and options.
%
%   The inputs left, right, and control can be either N-by-3 arrays of
%   data (see dmml_counts_table), filenames referring to .dmrb files, or
%   lists of the two.
%
%   The following options (default) are available:
%     'Sites' (nan) : Number of genomic sites to use. This is useful for
%       multiple comparisons to pad the data to a common size.
%     'Letters' (nan) : Number of elements in the alphabet. The value is
%       inferred from the data but can be specified to have a common size.
%     'Order' (1) : Number of sites to include in the local intersite
%       modeling. Larger number accounts for longer correlations, but is
%       computationally more expensive.
%     'Purity' (.5) : Initial purity values of the tumor samples. This can
%       be a vector of appropriate size.
%     'PurityPrec' (0) : Precision of the initial purity values in number
%        of samples.
%     'Err' (1e-3) : Initial error parameter value.
%     'ErrPrec' (0) : Precision of the initial error parameter in number of
%       samples.
%     'MaxIters' (25) : Maximum number of EM iterations.
%     'Resets' (10) : Number of QMC restarts.
%
%   The output is a D*(D-1)/2-by-M array of p-values of the pairwise
%   comparisons (the row order is as in pdist), for each of the M genomic
%   sites. Further outputs are in the output stats, which is a struct with
%   the following members:
%     'pvals' : The array of p-values as describe above.
%     'probs' : 2^(T+1)-by-M array of the "purified" methylation patterns,
%       where T is the number of tumor samples. The probability that the
%       methylation is z0 for the normal cells and z(i) for i=1..T for 
%       the T cancer cell types (one in each of the T tumor samples) at 
%       the site m is given by the element:
%         ( z0*2^0 + z(1)*2^1 + z(2)*2^2 + ..., m )
%     'purity' : A T-by-1 vector of purity estimates for the T samples.
%     'err' : An estimate of the error parameter.
%     'nlog' : Negative log-likelihood.
%
%   !!! This is a toy example, intented to be an example of building
%     more generalized models. In particular, the latent matrix cannot be
%     kept in memory for genome-scale problems. !!!

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
	
function [pvals, stats, latent]= dmml_test(leftright, control, varargin)
	%% normalize inputs
	% left, right, etc.
	if ~iscell(leftright)
		leftright= {leftright};
	end
	for k= 1:numel(leftright)
		leftright{k}= parseinput_(leftright{k});
	end
	
	% control
	if nargin <  2 || isempty(control)
		control= {};
	end
	control= parseinput_(control);
	
	%% handle options
	% default options-- USE lowerCASE !!
	options= struct('sites', nan, 'letters', nan, 'order', 1, ...
		'purity', .5, 'purityprec', 0, 'err', 1e-3, 'errprec', 0, ...
			'maxiters', 25, 'resets', 10);
	
	% parse
	for j= 1:2:numel(varargin)
		%% split
		[key, value]= deal(lower(varargin{j}), varargin{j+1});
		
		%% check
		assert(isfield(options, key), ...
			'The option ''%s'' is invalid.', varargin{j});
		assert(isequal(class(options.(key)), class(value)), ...
			'The option ''%s'' must be a %s.', key, class(options.(key)));
		
		%% store
		options.(key)= value;
		
	end
		
	%% set up dimensions
	% defaults
	sites= options.sites;
	letters= options.letters;
	order= options.order;
	
	% auto-detect sites?	
	if isnan(sites)
		%% find topmost site
		sites= max(cellfun(@(data)max(data(:, 1)), [leftright{:}, control]));
	end
	
	% auto-detect letters?
	if isnan(letters)
		%% find topmost letter
		letters= max(cellfun(@(data)max(data(:, 3)) + 1, [leftright{:}, control]));
	end
	
	%% read input data
	% read non-control inputs
	counts= cell(1, numel(leftright) + 1);
	for k= 1:numel(leftright)
		counts{k+1}= dmml_counts_table(sites, letters, order, zeros(0, 3));
		for kk= 1:numel(leftright{k})
			counts{k+1}= counts{k+1} + dmml_counts_table( ...
				sites, letters, order, leftright{k}{kk});
		end
	end
	
	% read control
	counts{1}= dmml_counts_table(sites, letters, order, zeros(0, 3));
	for kk= 1:numel(control)
		counts{1}= counts{1} + dmml_counts_table( ...
			sites, letters, order, control{kk});
	end
	
	%% run estimator
	% get initial parameters
	parms0= [ options.err * ones(1, options.resets+1); dmml_halton( ...
		options.resets+1, (1-options.purity(:).') .* ones(1, numel(leftright)) ) ];
	% get prior
	prior_means= parms0(:, 1).';
	prior_precs= [ options.errprec, options.purityprec(:).' .* ...
		ones(1, numel(leftright)) ];
	
	% estimate
	[pvals, probs, parms, nlogl, latent]= dmml_estimate_(sites, letters, order, ...
		counts, parms0(:, 1).', prior_means, prior_precs, options.maxiters);
	% apply resets
	for r= 1:options.resets
		[pvals1, probs1, parms1, nlogl1, latent1]= dmml_estimate_(sites, letters, order, ...
			counts, parms0(:, r+1).', prior_means, prior_precs, options.maxiters);
		if nlogl1 < nlogl
			[pvals, probs, parms, nlogl, latent]= deal(pvals1, probs1, parms1, nlogl1, latent1);
		end
	end
	
	%% create output
	stats= struct('pvals', pvals, 'probs', probs, ...
		'purity', 1 - parms(2:end), 'err', parms(1), 'nlog', nlogl);

function input= parseinput_(input)
	%% normalize structure
	if ~iscell(input)
		input= {input};
	end
	
	%% read files
	for j= 1:numel(input)
		if ischar(input{j})
			input{j}= dmrb_read(input{j});
		end
	end

function [pvals, probs, parms, nlogl, latent]= dmml_estimate_(sites, letters, ...
		order, counts, parms, prior_means, prior_precs, maxiters)
	%%
	%
	% Legend of the inputs:
	%
	%   sites: Number of genomic sites whose methylation is to be estimated
	%   letters: Alphabet size--this is 2 for binary methylation
	%   order: Order of intersite modeling
	%
	%   counts: Tabulated input data
	%
	%   parms: Initial parameters
	%   prior_means: Prior estimate of the initial parameters
	%   prior_precs: Precision of the prior parameter estimates
	%

	%
	% This function is not completely generic. It assumes a particular
	% topology, namely, that there is a pure normal cell sample and multiple
	% impure tumor samples, each of which consists of the normal cells and
	% cancer cells specific to the sample.
	%
	% Here, "track #1" denotes the methylation pattern of the normal cells,
	% while tracks k+1 for k=1..t-1 are for the t-1 distinct cancer cell
	% types. Similarly, parameter #1 is the error parameter, assumed to be
	% common in each sample, and parameters k+1 for k=1..t-1 are the
	% impurities of the t-1 cancer samples.
	%
	
	%% precompute
	% track descriptors--these are bitmaps which specify the tracks 
	%  that are included in each sample
	trackmasks= [ 1, 1 + 2 .^ (1:numel(counts)-1) ];
	% index for the error parameter
	errind= 1;
	% indices for mixing parameters
	alphainds= num2cell( 1:numel(counts) );
	alphainds{1}= [];
	
	% total number of tracks
	tracks= max(floor(log2(trackmasks) + 1));

	% precompute input translation tables
	intabs= cell(1, numel(counts));
	for k= 1:numel(counts)
		intabs{k}= dmml_input_table(letters, order, tracks, trackmasks(k));
	end
	
	%% loop
	% start with a uniform latent variable distribution
	latent= dmml_latent_uniform(sites, letters, order, tracks);
	
	for iter= 1:maxiters
		%% estimate distribution of latent variables
		% update distribution
		for k= 1:numel(counts)
			latent= latent + dmml_latent_update(intabs{k}, ...
				dmml_model_table(order, trackmasks(k), [parms(alphainds{k}), ...
					1-sum(parms(alphainds{k}))], parms(errind)), counts{k});
		end
		
		% normalize latent variable distribution
		[latent, scale]= dmml_latent_scale(latent);
		% nlogl is the negative full-model log-likelihood
		nlogl= -scale/order;
% fprintf(1, 'nlogl = %g\n', nlogl);
		
		%% re-estimate parameters
		% add prior information to the objective
		obj1= dmml_obj_prior(parms, prior_means, prior_precs);
		% add information from the expected log-likelihood of the data
		for k= 1:numel(counts)
			obj1= obj1 + dmml_obj_update(trackmasks(k), alphainds{k}, 1, ...
				parms, dmml_expect_update(latent, intabs{k}, counts{k}));
		end
		
		% get regularizer towards current parameter
		reg_obj1= dmml_obj_prior(parms, parms, ones(size(parms)));
		% search for a valid solution
		scale= 1;
		while scale > 0
			newparms= dmml_obj_solve(parms, scale*obj1 + (1-scale)*reg_obj1);
			if all(0 < newparms & newparms < 1)
				break;
			end
			scale= .5*scale;
		end
		
		%% update parameters
		parms= newparms;
% fprintf(1, 'new params: fhat= %g, ahats= [%g, %g]\n', ...
% 	parms(errind), 1-parms(2:end));
		
	end
	
	%% do a final upgrade pass
	% update latent distribution
	for k= 1:numel(counts)
		latent= latent + dmml_latent_update(intabs{k}, ...
			dmml_model_table(order, trackmasks(k), [parms(alphainds{k}), ...
				1-sum(parms(alphainds{k}))], parms(errind)), counts{k});
	end
	
	% normalize & compute likelihood 	
	[latent, scale]= dmml_latent_scale(latent);
	% nlogl is the negative full-model log-likelihood
	nlogl= -scale/order;
% fprintf(1, 'nlogl = %g\n', nlogl);

	%% get marginal distribution
	% get model likelihood
	like= dmml_latent_uniform(sites, letters, order, tracks);
	for k= 1:numel(counts)
		like= like + dmml_latent_update(intabs{k}, ...
			dmml_model_table(order, trackmasks(k), [parms(alphainds{k}), ...
				1-sum(parms(alphainds{k}))], parms(errind)), counts{k});
	end
	
	% get marginal distribution (1-D) of the methylation patterns
	%  from the full neighborhood distribution model
	probs= exp(dmml_marginal_table(letters, tracks, dmml_latent_scale(like)));
	
	% get likelihoods
	marg_like= dmml_marginal_table(letters, tracks, like);
	% log-likelihood of the alternative model
	l1= logsumexp_(marg_like);
	
	% compute likelihood ratios
	d= numel(counts)-1;
	lrstats= zeros(d*(d-1)/2, sites); % lrstat = -2*log(LR)
	for k= 1:d
		for kk= k+1:d
			% sum over the marginal where the appropriate methylation patterns 
			%  match to get the alternative model probability
			lrstats((k-1)*(d-k/2)+kk-k, :)= -2 * (( logsumexp_(marg_like( ...
				(( mod(fix( (0:letters^tracks-1)/letters^k ), letters) == ...
					mod(fix( (0:letters^tracks-1)/letters^kk ), letters) )), :)) - l1 ));
		end
	end
	
	% get p-values
	pvals= dmml_chi2cdf(lrstats, letters-1, 'upper');
	
function y= logsumexp_(x)
	% computes log(sum(x, 1)) in a more stable way
	p= max(x, [], 1);
	y= bsxfun(@plus, p, log(sum(exp(bsxfun(@minus, x, p)), 1)));
