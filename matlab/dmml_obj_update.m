
% DMML_OBJ_UPDATE Add objective information from the expected likelihood
%   obj= DMML_OBJ_UPDATE(trackmask, alphainds, errind, parms, expect) and the
%   information from expect to update the objective. Here, alphainds are the
%   indices of the free mixing parameters in parms and errind that of the
%   error.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
