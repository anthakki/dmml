
% DMML_LATENT_UPDATE Update latent variable distribution
%   latent= DMML_LATENT_UPDATE(inputtab, modeltab, counts) computes the update
%   for the latent variable distribution.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
