
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/solver.h>
#include <mex.h>
#include <string.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

/* Computes number of set bits in x */
extern int popcntl(unsigned long x);

static
int
solveOrder(mwSize *order, mwSize modellen, mwSize trackmask)
{
	unsigned long base, len;
	
	/* Get base */
	base = (1lu << popcntl(trackmask)) + 1;

	/* Find order */
	len = base;
	for (*order = 1; len < modellen; ++*order)
	{
		/* Multiply, check overflow */
		if ((unsigned long)-1 / len < base)
			return 0;
		len *=  base;
	}

	return len == modellen;
}

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; }

static
int
checkCount(mwSize *count, double value)
{
	*count = (mwSize)value;
	return *count >= 0 && *count == value;
}

static
int
isVector(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetM(arg) ||
		mxGetNumberOfElements(arg) == mxGetN(arg); }

#define MEXFILENAME "dmml_obj_update"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize trackmask, numinds, dim, modellen, order, i, ind;
	const double *origalphainds, *parms, *expect;
	double errind;
	uint32_T inds[32];
	double *obj;

	/* Check input count */
	if (nrhs < 5)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 5)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Suppress warnings */
	trackmask = 0;

	/* Check inputs */
	if (!(isFullReal(prhs[0]) && mxIsDouble(prhs[0]) && isScalar(prhs[0]) && checkCount(&trackmask, *mxGetPr(prhs[0]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":trackmask", "The input TRACKMASK must be a full real scalar double representing a non-negative integer.");
	if (!(isFullReal(prhs[1]) && mxIsDouble(prhs[1]) && isVector(prhs[1])))
		mexErrMsgIdAndTxt(MEXFILENAME ":alphainds", "The input ALPHAINDS must be a non-empty full real double vector.");
	if (!(isFullReal(prhs[2]) && mxIsDouble(prhs[2]) && isScalar(prhs[2])))
		mexErrMsgIdAndTxt(MEXFILENAME ":errind", "The input ERRIND must be a scalar full real double.");
	if (!(isFullReal(prhs[3]) && mxIsDouble(prhs[3]) && isVector(prhs[3])))
		mexErrMsgIdAndTxt(MEXFILENAME ":parms", "The input PARMS must be a full real double vector.");
	if (!(isFullReal(prhs[4]) && mxIsDouble(prhs[4]) && isVector(prhs[4])))
		mexErrMsgIdAndTxt(MEXFILENAME ":expect", "The input EXPECT must be a full real double vector.");

	/* Get data */
	origalphainds = mxGetPr(prhs[1]);
	numinds = mxGetNumberOfElements(prhs[1]) + 1;
	errind = *mxGetPr(prhs[2]);
	parms = mxGetPr(prhs[3]);
	dim = mxGetNumberOfElements(prhs[3]);
	expect = mxGetPr(prhs[4]);
	modellen = mxGetNumberOfElements(prhs[4]);

	/* Check dimension constraints */
	if (!(numinds == (mwSize)popcntl(trackmask)))
		mexErrMsgIdAndTxt(MEXFILENAME ":inds", "The input INDS has incompatible number of elements.");
	if (numinds > countof(inds))
		mexErrMsgIdAndTxt(MEXFILENAME ":inds", "The input INDS is too long.");
	if (!solveOrder(&order, modellen, trackmask))
		mexErrMsgIdAndTxt(MEXFILENAME ":expect", "The input EXPECT has incompatible number of elements.");

	/* Check & convert indices */
	for (i = 0; i < numinds - 1; ++i)
	{
		/* Check */
		if (!(checkCount(&ind, origalphainds[i]) && 1 <= ind && ind <= dim))
			mexErrMsgIdAndTxt(MEXFILENAME ":inds", "The input ALPHAINDS contains invalid values.");
		/* Convert */
		inds[i] = ind - 1; /* NOTE: MATLAB uses 1-based indices */
	}
	{
		/* Check */
		if (!(checkCount(&ind, errind) && 1 <= ind && ind <= dim))
			mexErrMsgIdAndTxt(MEXFILENAME ":errind", "The input ERRIND has invalid value.");
		/* Convert */
		inds[numinds-1] = ind - 1;
	}

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(dmml_obj_size(dim), 1, mxREAL);
	obj = mxGetPr(plhs[0]);
	/* NOTE: mxCreateDoubleMatrix() zeroes obj */

	/* Compute objective */
	dmml_obj_update(obj, dim, order, trackmask, &inds[0], inds[numinds-1], parms, expect);
}
