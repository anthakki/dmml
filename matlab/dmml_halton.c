
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <dmml/halton.h>
#include <mex.h>

static
int
isFullRealDouble(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsDouble(arg); }

static
int
isVector(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetM(arg) ||
		mxGetNumberOfElements(arg) == mxGetN(arg); }

static
int
checkSize(mwSize *size, double value)
{
	/* Cast & compare */
	*size = (mwSize)value;
	return *size >= 0 && *size == value;
}

#define MEXFILENAME "dmml_halton"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize count, dim, i, k;
	const double *seed;
	double *points;

	/* Check input arguments */
	if (nrhs < 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check inputs */
	if (!(isFullRealDouble(prhs[0]) && checkSize(&count, *mxGetPr(prhs[0]))))
		mexErrMsgIdAndTxt(MEXFILENAME ":count", "The input COUNT must be a full real double scalar representing an integer.");
	if (!(isFullRealDouble(prhs[1]) && isVector(prhs[1])))
		mexErrMsgIdAndTxt(MEXFILENAME ":seed", "The input SEED must be a full real double vector.");

	/* Get data */
	seed = mxGetPr(prhs[1]);
	dim = mxGetNumberOfElements(prhs[1]);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(dim, count, mxREAL);
	points = mxGetPr(plhs[0]);

	/* Compute */
	for (i = 0; i < count; ++i)
		for (k = 0; k < dim; ++k)
			points[i*dim + k] = dmml_halton(i, k, seed[k]);
}
