% DMML(1) Version 0.9 | dmml documentation

NAME
====

**dmml** -- Differential methylation using a maximum-likelihood method

SYNOPSIS
========

| **dmml** \[_options_\] \[_m_:\]_left.dmrb_ \[_m_:\]_right.dmrb_ \[\[_m_:\]_control.dmrb_ \[...\]\]

DESCRIPTION
===========

**dmml** uses a maximum-likelihood method to identify differentially methylated sites in two or more tumor samples whose tumor cell fraction is unknown.

Options
-------

--order=_k_

:   Specifies the order _k_ of local comethylation modeling (default: 1). Higher order is expected to result in more accurate estimates, but increase the computational effort exponentially. Typical values are 1 to 3, but up to 10 might be feasible for small problems.

--sites=\[_a_:\]_b_

:   Specifies a subset of the sites from _a_ (default: 1) to _b_ (default: automatic) to be used in the analysis (1-based, inclusive). The default is to model site from 1 up to largest site index encountered in any of the input files. This might be inconvenient if multiple comparisons are made, which cover different regions of genome, and can be overridden by specifying _b_. Alternatively, subsets of the genome can be analyzed separately by specifying both _a_ and _b_.

--error=_f_\[:_fp_\]

:   Sets the initial value of the error parameter _f_ (default: 0.001) and optionally its prior precision _fp_ in samples (default: 0). The error parameter specifies the fraction of data unexplained by the mixture model and corresponds to errors such as conversion, sequencing, and alignment errors and small amounts of cellular heterogeneity. The prior can be used to add external information about the parameter, 0 being none and a very high value essentially fixing _f_.

--purity-left=_p_\[:_pp_\], --purity-right=_p_\[:_pp_\]

:   Sets the initial tumor purity estimates _p_ of the two first samples (default: 0.5) and optionally their prior precision _pp_ (default: 0). The purity means the fraction of cancer DNA of interest, while the rest of the DNA is assumed to be from the normal cells.

--max-iters=_i_

:   Specifies the maximum number of expectation maximization iterations per restart _i_ (default: 25). For very large problems or for more accurate parameter estimates a higher number of iterations is recommended. For genome scale data, more than 1000 iterations rarely benefits anything.

--min-delta=_d_

:   Specifies the minimum improvement _d_ in the objective that is not considered a stall (default: 1.49e-8). This value can be increased for earlier halt (faster optimization) at the expense of a lower accuracy.

--resets=_r_

:   Specifies the number of quasi-Monte Carlo resets _r_ (default: 10). A large number of resets facilitates the algorithm to explore the whole state-space and not get stuck at a local minimum. For comparison of high number of samples simultaneously, a larger number of resets might be needed.

--output-params=\[_m_:\]_fn_

:   Outputs the negative log-likelihood, the estimated error parameter, and the purity of each tumor sample in the file _fn_. The parameter _m_ and can be 'a' or 'b' to specify that the output is in ASCII or binary.

--output-probs=\[_m_:\]_fn_

:   Outputs the joint distributions of the latent methylation patterns in the file _fn_. The parameter _m_ specifies that the output is ASCII ('a') or binary ('b').

--output-pvalues=\[_m_:\]_fn_

:   Outputs the p-values of the two-tailed hypothesis tests that the methylation patterns are equal between each pair of the tumor samples at a specific site. The parameter _m_ specifies that the output is ASCII ('a') or binary ('b'). For multiple comparisons, these p-values should be further adjusted accordingly. 

Inputs
------

Each of the input files must be a sorted binary file as described in the manual page of **dmrtool** and produced by **dmrtool view -b**.

At least two inputs are expected: _left.dmrb_ and _right.dmrb_. By default, these are assumed to be the (impure) tumor samples to be compared. If further samples are provided, they are assumed to be (pure) controls.

More complex settings can be by specifying the track bitmaps _m_. The track bitmap is a decimal number with the _j_:th bit set of the tumor sample is assumed to contain (an unknown fraction of) cells of type _j_. Up to 32 tracks are supported, first of which is the control. The defaults for _left.dmrb_, _right.dmrb_, and _control.dmrb_ are 3, 5, and 1, respectively. The following does a 5-way comparison with 5 types of cancer cells and a common control:

|   **dmml** a.dmrb b.dmrb control.dmrb 9:c.dmrb 17:d.dmrb 33:e.dmrb

OUTPUT FILES
============

The output files specified through the various output switches trigger outputs to be written in the specified files. By default, no output is written.

Each output can specify an optional file mode _m_ which can be used to select between ASCII (printable; 'a') or binary ('b') output. If **dmml** is compiled with `zlib` support, the each output file can be optionally gzip-compressed.

The ASCII files are tab-separated plain text files containing the decimal representation of the parameters. These file contain a header row labeling the columns where applicable. Meanwhile, the binary files consist of native double-precision floating-point numbers in native endianess, which can be extracted using **pd2asc**.

The output file specified by **--output-params** contains a 1-by-(2+t) vector of model parameters (negative log-likelihood, the error parameter, and the tumor purities purity[1], ..., purity[t] in each sample). The methylation patterns of the pure cell types are included in the file specified by **--output-probs**, which is a 2^(t+1)-by-s (or, rather, a 2-by-2-by-...-s) column-major array representing the joint distribution of the underlying methylation patterns. The columns correspond the s sites, and the values probabilities (each column sums to 1). The first row corresponds to (Z0,Z1,Z2,...,Zt)= (0,0,0,...), the second is (1,0,0,...), and the last (1,1,1,...), where Z0 is the methylation of the control, and Zt that of the t:th cancer cells. The probabilities for a single pattern being methylated can be obtained by summing over the columns where the appropriate variable is 1. The output specified by **--output-pvalues** is a t\*(t-1)/2-by-s column-major matrix of p-values of the pairwise differential methylation tests between the tumor samples ((1,2),(1,3),...,(t-1,t)). In the above, t represents the number of samples to be compared (2 for a pairwise comparison of two tumor samples regardless if a control is used or not) and s is the number of sites (as specified by **--sites**).

EXAMPLES
========

The most common scenario is a two-way comparison of two tumor samples, either with or without a control. Two-way comparison, no prior information on the parameters and no control:

|   **dmml** a.dmrb b.dmrb

In the above, the error parameter and purities are automatically estimated along with the methylation patterns of both cancer cell types and the normal cells.

Two-way comparison, no prior information but using a control:

|   **dmml** a.dmrb b.dmrb control.dmrb

Two-way comparison, known tumor purities _p_ and _q_ for the samples _a.dmrb_ and _b.dmrb_, respectively:

|   **dmml** --purity-left=_p_:1e100 --purity-right=_q_:1e100 a.dmrb b.dmrb

BUGS
====

More flexible settings can be implemented using the C or MATLAB API.

SEE ALSO
========

**dmrtool**, **pd2asc**, a MATLAB API is available

AUTHOR
======

Antti Hakkinen.
