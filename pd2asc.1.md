% PD2ASC(1) Version 0.9 | dmml documentation

NAME
====

**pd2asc** -- Convert packed doubles into ASCII

SYNOPSIS
========

| **pd2asc** \[**--fieldnames=**_f1_\[,_f2_\[,...\]\]\] \[**--width=**_w_] [_input.pd_ \[...\]\]

DESCRIPTION
===========

The tool reads native double-width floating point numbers in binary form (this is convenient for large and accurate results) from the standard input or the input files and converts them into tab-separated plain text files.

Options
-------

--fieldnames=_f1_[,_f2_[,...]]

:   Specifies the field names in the header of the output

--width=_w_

:   Specifies the number of items per row

BUGS
====

Compressed files are not supported transparently.

SEE ALSO
========

dmml

AUTHOR
======

Antti Hakkinen.
