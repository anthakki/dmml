
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmml/chi2cdf.h"
#include "dmml/halton.h"
#include "dmml/model.h"
#include "dmml/parser.h"
#include "dmml/solver.h"
#include "dmrb.h"
#include "imath.h"
#include "tablewriter.h"
#include <float.h> /* HUGE_VAL */
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
void
reverseargs(char **argv, int argc)
{
	int i;
	char *tmp;

	/* Loop through first half of the arguments */
	for (i = 0; i < argc / 2; ++i)
	{
		/* Swap */
		tmp = argv[i];
		argv[i] = argv[argc - (i + 1)];
		argv[argc - (i + 1)] = tmp;
	}
}

static
int
sortargs(char **argv, int argc)
{
	int half, a, b;

	/* Trivial case */
	if (argc < 2)
		return argc > 0 && argv[0][0] == '-';

	/* Divide */
	half = argc / 2;
	a = sortargs(&argv[0], half);
	b = sortargs(&argv[half], argc - half);

	/* Rotate */
	reverseargs(&argv[a], half - a + b);
	reverseargs(&argv[a], b);
	reverseargs(&argv[a + b], half - a);

	return a + b;
}

struct options {
	/* Problem dimensions: neighborhood size */
	uint8_t order;

	/* Site options: sites site_offset+[0,sites) are covered */
	uint32_t site_offset, sites;
	int sites_set; /* don't auto-detect sites? */

	/* Parameters: error, tumor purities for first two tumor samples;
	 *   and precision of each */
	double error, purity1, purity2;
	double error_prec, purity1_prec, purity2_prec;

	/* Optimization parameters */
	unsigned long max_iters;
	double min_delta;
	unsigned long resets;

	/* Output settings: filename, mode: 'a' for ASCII, 'b' for binary */
	const char *output_latent;
	int output_latent_mode;
	const char *output_params;
	int output_params_mode;
	const char *output_probs;
	int output_probs_mode;
	const char *output_pvalues;
	int output_pvalues_mode;

}; /* struct options */

static
void
defaultopts(struct options *opts)
{
	/* Order */
	opts->order = 1;

	/* Set site options */
	opts->site_offset = 0;
	opts->sites = 0;
	opts->sites_set = 0;

	/* Parameters */
	opts->error = 1e-3;
	opts->purity1 = .5;
	opts->purity2 = .5;

	/* Parameter precision */
	opts->error_prec = 0.;  /* Haldane prior */
	opts->purity1_prec = 0.;
	opts->purity2_prec = 0.;

	/* Stopping condition */
	opts->max_iters = 25;
	opts->min_delta = 1.49e-8; /* sqrt(eps('double')) */
	opts->resets = 10;

	/* Output settings */
	opts->output_latent = NULL;
	opts->output_latent_mode = 'a';
	opts->output_params = NULL;
	opts->output_params_mode = 'a';
	opts->output_probs = NULL;
	opts->output_probs_mode = 'a';
	opts->output_pvalues = NULL;
	opts->output_pvalues_mode = 'a';

}

static
int
strpiececmp(const char *s1, size_t l1, const char *s2)
{
	size_t i;

	/* Compare up to a common length */
	for (i = 0; i < l1 && s2[i] != '\0'; ++i)
		if (s1[i] != s2[i])
			return (s1[i] > s2[i]) - (s1[i] < s2[i]);

	return (i < l1) - (s2[i] != '\0');
}

static
int
parseargs(struct options *opts, int argc, char **argv)
{
	int optind, i;
	double d1, d2;
	unsigned long l1, l2;
	int i1, i2, i3, i4, i5;
	char sentinel;

	/* Partition arguments into options/non-options */
	optind = sortargs(&argv[1], argc - 1) + 1;

	/* Parse options */
	for (i = 1; i < optind; ++i)
		if (argv[i][1] == '-')
			switch (argv[i][2])
			{
				case 'e':
					if (sscanf(argv[i], "--error=%lf:%lf %c", &d1, &d2, &sentinel) == 2)
					{
						opts->error = d1;
						opts->error_prec = d2;
						break;
					}
					if (sscanf(argv[i], "--error=%lf %c", &d1, &sentinel) == 1)
					{
						opts->error = d1;
						break;
					}
					goto invalid_opt;

				case 'm':
					if (sscanf(argv[i], "--max-iters=%lu %c", &l1, &sentinel) == 1)
					{
						opts->max_iters = l1;
						break;
					}
					if (sscanf(argv[i], "--min-delta=%lf %c", &d1, &sentinel) == 1)
					{
						opts->min_delta = d1;
						break;
					}
					goto invalid_opt;

				case 'o':
					if (sscanf(argv[i], "--order=%lu %c", &l1, &sentinel) == 1)
					{
						opts->order = (uint8_t)l1;
						break;
					}
					if (sscanf(argv[i], "--output-%n%*[^=]%n=%n%*[^:]%n:%n %c",
							&i1, &i2, &i3, &i4, &i5, &sentinel) == 1)
					{
						int *modep;

					parse_output:
						if (strpiececmp(&argv[i][i1], i2-i1, "latent") == 0)
						{
							opts->output_latent = &argv[i][i5];
							modep = &opts->output_latent_mode;
						}
						else if (strpiececmp(&argv[i][i1], i2-i1, "params") == 0)
						{
							opts->output_params = &argv[i][i5];
							modep = &opts->output_params_mode;
						}
						else if (strpiececmp(&argv[i][i1], i2-i1, "probs") == 0)
						{
							opts->output_probs = &argv[i][i5];
							modep = &opts->output_probs_mode;
						}
						else if (strpiececmp(&argv[i][i1], i2-i1, "pvalues") == 0)
						{
							opts->output_pvalues = &argv[i][i5];
							modep = &opts->output_pvalues_mode;
						}
						else
							goto invalid_opt;

						if (strpiececmp(&argv[i][i3], i4-i3, "") == 0)
							/* NOP */ ;
						else if (strpiececmp(&argv[i][i3], i4-i3, "binary") == 0 ||
								strpiececmp(&argv[i][i3], i4-i3, "b") == 0)
							*modep = 'b';
						else if (strpiececmp(&argv[i][i3], i4-i3, "ascii") == 0 ||
								strpiececmp(&argv[i][i3], i4-i3, "a") == 0)
							*modep = 'a';
						else
						{
							fprintf(stderr, "%s: invalid mode \"%.*s\" for --output-%.*s" "\n",
								argv[0], i4-i3, &argv[i][i3], i2-i1, &argv[i][i1]);
							return -1;
						}

						break;
					}
					if (sscanf(argv[i], "--output-%n%*[^=]%n=%n%n%n %c",
							&i1, &i2, &i3, &i4, &i5, &sentinel) == 1)
						goto parse_output;
					goto invalid_opt;

				case 'p':
					if (sscanf(argv[i], "--purity-left=%lf:%lf %c", &d1, &d2, &sentinel) == 2)
					{
						opts->purity1 = d1;
						opts->purity1_prec = d2;
						break;
					}
					if (sscanf(argv[i], "--purity-left=%lf %c", &d1, &sentinel) == 1)
					{
						opts->purity1 = d1;
						break;
					}
					if (sscanf(argv[i], "--purity-right=%lf:%lf %c", &d1, &d2, &sentinel) == 2)
					{
						opts->purity2 = d1;
						opts->purity2_prec = d2;
						break;
					}
					if (sscanf(argv[i], "--purity-right=%lf %c", &d1, &sentinel) == 1)
					{
						opts->purity2 = d1;
						break;
					}
					goto invalid_opt;

				case 'r':
					if (sscanf(argv[i], "--resets=%lu %c", &l1, &sentinel) == 1)
					{
						opts->resets = l1;
						break;
					}

				case 's':
					if (sscanf(argv[i], "--sites=%lu:%lu %c", &l1, &l2, &sentinel) == 2)
					{
						opts->site_offset = l1 - 1;
						opts->sites = l2 - (l1 - 1);
						opts->sites_set = 1;
						break;
					}
					if (sscanf(argv[i], "--sites=%lu %c", &l1, &sentinel) == 1)
					{
						opts->site_offset = 0;
						opts->sites = l1;
						opts->sites_set = 1;
						break;
					}
					goto invalid_opt;

				default:
					/* FALLTHROUGH */

				invalid_opt:
					fprintf(stderr, "%s: invalid option %s" "\n", argv[0], argv[i]);
					return -1;
			}
		else
			goto invalid_opt;

	/* Check number of samples */
	if (argc - optind < 2)
	{
		fprintf(stderr, "%s: missing inputs" "\n", argv[0]);
		return -1;
	}

	return optind;
}

static
void
parseinput(const char **filename, uint32_t *trackmask, const char *argvi, int i)
{
	unsigned long mask;
	int name_p;

	/* Get default mask */
	switch (i)
	{
		case 0: mask = 3; break;
		case 1: mask = 5; break;
		default: mask = 1; break;
	}

	/* Parse mask */
	name_p = 0;
	(void)sscanf(argvi, "%lu:%n", &mask, &name_p);

	/* Populate outputs */
	*filename = &argvi[name_p];
	*trackmask = (uint32_t)mask;
}

static
const char *
parsetrackmask(int *index, uint32_t trackmask)
{
	int i;

	/* Check that the control cell type is involved */
	*index = -1;
	if ((trackmask & 1u) == 0)
		return "unsupported topology";

	/* Find sample type index */
	*index = 0;
	for (i = 1; i < CHAR_BIT * (int)sizeof(trackmask); ++i)
		if (((trackmask >> i) & 1u) != 0)
		{
			/* Two tumor cell types */
			if (*index > 0)
				return "unsupported topology";

			*index = i;
		}

	return NULL;
}

static
void
usage(const char *argv0)
{
	/* Print usage */
	fprintf(stderr,
"Usage: %s [options] [m:]left.dmrb [m:]right.dmrb [[m:]control.dmrb [...]]" "\n"
"Options:" "\n"
"  --order=k      --error=f[:fp]         --max-iters=i  --output-params=[m:]fn   " "\n"
"  --sites=[a:]b  --purity-left=p[:pp]   --min-delta=d  --output-probs=[m:]fn    " "\n"
"                 --purity-right=p[:pp]  --resets=r     --output-pvalues=[m:]fn  " "\n"
		, argv0);
}

static
const char *
readdmrlimits(dmrb_record_t *limits, const char *filename)
{
	dmrb_reader_t reader;

	/* Open file */
	if (dmrb_reader_init(&reader, filename) != 0)
		return "failed to open for read";

	/* Get limits */
	memcpy(limits, dmrb_reader_limits(&reader), sizeof(*limits));

	/* Close file */
	dmrb_reader_deinit(&reader);

	return NULL;
}

struct problem {
	/* Number of sites modeleted */
	size_t sites;
	/* Alphabet size, local neighborhood size, number of cell types */ 
	uint8_t letters, order, tracks;
	/* Dimensions of the latent state, & input */ 
	size_t cols, rows, matchcombs;
	/* Size/stride for the input counts & transformation flags */
	size_t counts_stride, flags_stride;
};

static
void
init_problem(struct problem *problem, size_t sites, uint8_t letters, uint8_t order, uint8_t tracks)
{
	size_t input_size;

	/* Store elementary dimensions */
	problem->sites = sites;
	problem->letters = letters;
	problem->order = order;
	problem->tracks = tracks;

	/* Compute state dimensions */
	problem->cols = dmml_latent_cols(letters, order, tracks);
	problem->rows = dmml_latent_rows(sites, order);
	problem->matchcombs = dmml_input_size(letters, order);
	input_size = dmml_input_size(letters, order);

	/* Compute lookup table dimensions */
	problem->counts_stride = input_size * problem->rows;
	problem->flags_stride = dmml_latent_cols(letters, order, tracks) * input_size;
}

static
const char *
readdmrcounts(uint32_t *counts, uint32_t site_offset, const struct problem *problem, const char *filename)
{
	dmrb_reader_t reader;
	uint32_t sites;
	uint8_t letters;
	dmml_parser_t parser;
	int reset;
	uint32_t lastread;
	dmrb_record_t record;
	int warn;

	/* Open file */
	if (dmrb_reader_init(&reader, filename) != 0)
		return "failed to open as a .dmrb file";

	/* Get dimensions */
	sites = problem->sites;
	letters = problem->letters;

	/* Set up a parser */
	dmml_parser_init(&parser, letters, problem->order);
	reset = 1;
	lastread = 0;

	/* Loop */
	warn = 0;
	while (dmrb_reader_read(&reader, &record, 1) > 0)
	{
		/* Reset? */
		if (!reset && record.read != lastread)
			reset = 1;

		/* Push */
		if (dmml_parser_push(&parser, counts, site_offset, sites, reset, record.site, record.value % letters) != 0)
			warn = 1;

		/* Set read */
		reset = 0;
		lastread = record.read;
	}

	/* Print warning? */
	if (warn)
		fprintf(stderr, "warning: \"%s\" contains sites that are not increasing in a group" "\n", filename);
	/* Check for read errors */
	if (!(dmrb_reader_eof(&reader) > 0))
	{
		dmrb_reader_deinit(&reader);
		return "read error";
	}

	/* Close file */
	dmrb_reader_deinit(&reader);

	/* Flush */
	dmml_parser_flush(&parser, counts, site_offset, sites);

	return NULL;
}

static
void
update_models(double *models, size_t model_stride, const struct problem *problem, int inputs, const double *parms, const uint32_t *trackmasks)
{
	double alphas[2];
	int i;

	/* Loop through the inputs */
	for (i = 0; i < inputs; ++i)
	{
		/* Get mixing parameters */
		alphas[0] = (&parms[1])[i];
		alphas[1] = 1. - alphas[0];

		/* Update the model table */ 
		dmml_model_table(&models[i * model_stride], problem->order, trackmasks[i], alphas, parms[0]);
	}
}

static
void
get_models(double *models, size_t model_stride, const struct problem *problem, int inputs, const double *parms , const uint32_t *trackmasks)
{
	/* Zero models */
	memset(models, 0, inputs * model_stride * sizeof(*models));
	/* Accumulate */
	update_models(models, model_stride, problem, inputs, parms, trackmasks);
}

static
void
update_latent(double *latent, const struct problem *problem, int inputs, const uint32_t *counts, size_t counts_stride, const uint32_t *flags, size_t flags_stride, double *models, size_t model_stride)
{
	int i;

	/* Loop through the inputs */
	for (i = 0; i < inputs; ++i)
		/* Update latent state */
		dmml_latent_update(latent, 1, problem->cols, problem->matchcombs, &flags[i * flags_stride], &models[i * model_stride], &counts[i * counts_stride]);
}

static
void
get_unscaled_latent(double *latent, const struct problem *problem, int inputs, const uint32_t *counts, size_t counts_stride, const uint32_t *flags, size_t flags_stride , double *models, size_t model_stride)
{
	/* Zero latent state */
	memset(latent, 0, problem->cols * sizeof(*latent));
	/* Apply updates */
	update_latent(latent, problem, inputs, counts, counts_stride, flags, flags_stride, models, model_stride );
}

static
double
get_latent(double *latent, const struct problem *problem, int inputs, const uint32_t *counts, size_t counts_stride, const uint32_t *flags, size_t flags_stride, double *models, size_t model_stride)
{
	/* Get latent */
	get_unscaled_latent(latent, problem, inputs, counts, counts_stride, flags, flags_stride, models, model_stride);
	/* Scale */
	return dmml_latent_scale(latent, 1, problem->cols);
}

static
int
endswith(const char *str, const char *suffix)
{
	size_t l1, l2;

	/* GEt lengths */
	l1 = strlen(str);
	l2 = strlen(suffix);

	/* Compare */
	if (l1 < l2)
		return 0;
	else
		return memcmp(&str[l1-l2], suffix, l2) == 0;
}

static
const char *
modestr( const char *filename, int mode) 
{
	static const char *const modes[2][2] = { { "w", "wz" }, { "wb", "wbz" } };

	/* Figure out the mode */
	return modes[mode == 'b'][endswith(filename, ".gz")];
}

struct alloc {
	struct {
		/* Pointer to the pointer receiving the block */
		void **dest; 
		/* Offset from the beginning of the block */
		size_t offset;
	} nodes[32];

	/* Number of nodes, bytes needed */
	int top;
	size_t used;
};

static
void
alloc_init(struct alloc *self)
{
	/* Zero counts */
	self->top = 0;
	self->used = 0;
}

#define alloc_push(self, dest, count, size) \
	(alloc_push_((self), (void **)(dest), (count), (size)))

static
void
alloc_push_(struct alloc *self, void **dest, size_t count, size_t size)
{
	size_t width, align, offset;

	/* Compute total size */ 
	width = count * size;

	/* Compute alignment */
	align = 16; /* TODO: <-- */
	if (size > align)
		align = size;

	/* Align offset */
	offset = (self->used + (align-1)) & ~(align-1);

	/* Push node */
	self->nodes[self->top].dest = dest;
	self->nodes[self->top].offset = offset;

	/* Update top */
	++self->top;
	self->used = offset + width;
}

static
void *
alloc_go(struct alloc *self)
{
	char *data;
	int i;

	/* Allocate */
	data = (char *)calloc(self->used, 1);
	if (data == NULL)
		return NULL;

	/* Store pointers */
	for (i = 0; i < self->top; ++i)
		*self->nodes[i].dest = data + self->nodes[i].offset;

	return data;
}

static
double
logaddexp(double x, double y)
{
	/* Computes log(exp(x) + exp(y)) but avoids underflow */
	if (x < y)
		return y + log(1. + exp(x - y));
	else
		return x + log(1. + exp(y - x));
}

int
main(int argc, char **argv)
{
	struct options opts;
	int optind, noncontrols, has_control, has_tumor2, inputs;
	uint32_t combined_mask;
	struct problem problem;
	uint32_t trackmasks[32], *counts, *flags;
	size_t model_stride;
	void *handle;
	double *latent, *models, *best_lut, *expect;
	double best_parms[32 + 1], best_logl;

	/* Parse options */
	{{
		/* Create default options */
		defaultopts(&opts);
	
		/* Parse arguments */
		optind = parseargs(&opts, argc, argv);
		if (optind < 0)
		{
			/* Show usage */
			usage(argv[0]);
			return EXIT_FAILURE;
		}
	}}

	/* Parse inputs, validate topology, & find limits from the data */ 
	{{ 
		uint64_t max_letters;
		uint32_t max_sites;
		int i, index;
		const char *filename, *errmsg;
		uint32_t mask;
		dmrb_record_t limits;
	
		/* Default limits */
		max_letters = 2;
		max_sites = 0;

		/* Number of samples */
		noncontrols = 2;
		has_control = 0;
		combined_mask = 0;

		/* Loop through the inputs */
		for (i = optind; i < argc; ++i)
		{
			/* Parse input */
			parseinput(&filename, &mask, argv[i], i - optind);

			/* Get sample index */
			errmsg = parsetrackmask(&index, mask);
			if (errmsg != NULL)
				goto input_error;

			/* Update number of samples */ 
			if (index > noncontrols)
				noncontrols = index;
			else
				has_control = 1;

			/* Update mask of used tracks */
			combined_mask |= mask;
	
			/* Get topmost site */
			errmsg = readdmrlimits(&limits, filename);
			if (errmsg != NULL)
			{
input_error:
				/* Print error */
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], filename, errmsg);
				return EXIT_FAILURE;
			}
	
			/* Update limits */
			if (limits.site > max_sites)
				max_sites = limits.site;
			if (limits.value > max_letters)
				max_letters = limits.value;
		}

		/* Create a new problem */
		init_problem(&problem, opts.sites_set ? opts.sites : max_sites,
			max_letters, opts.order, noncontrols + 1);

		/* Write out unified track masks */
		for (i = 0; i < noncontrols; ++i)
			trackmasks[i] = (1u | (1u << (i + 1)));
		if (has_control)
			trackmasks[noncontrols] = 1u;

		/* Tumor #2 sample is not degenerate */
		has_tumor2 = (combined_mask & ~3u) != 0 || opts.purity2_prec > 0.;

		/* If one of the main tumor samples are missing, inject a small
		   prior to keep the problem well posed */
		if ((combined_mask & 2u) == 0 && opts.purity1_prec == 0.)
			opts.purity1_prec = 1e-6;
		if ((combined_mask & 4u) == 0 && opts.purity2_prec == 0.)
			opts.purity2_prec = 1e-6;

		/* Number of input arrays */
		inputs = noncontrols + has_control;

		/* Compute size for each model table; in our topology, a constant */
		model_stride = dmml_model_size(problem.order, trackmasks[0]);

	}}

	/* Allocate memory */
	{{
		struct alloc alloc;

		/* Create an allocator context */
		alloc_init(&alloc);

		/* Schedule allocations */
		alloc_push(&alloc, &counts, inputs * problem.counts_stride, sizeof(*counts));
		alloc_push(&alloc, &latent, problem.cols * problem.order, sizeof(*latent));
		alloc_push(&alloc, &flags, inputs * problem.flags_stride, sizeof(*flags));
		alloc_push(&alloc, &models,  inputs * model_stride, sizeof(*models));
		alloc_push(&alloc, &expect, inputs * model_stride, sizeof(*expect));
		alloc_push(&alloc, &best_lut, inputs * model_stride, sizeof(*best_lut));

		/* Allocate memory */
		handle = alloc_go(&alloc);
		if (handle == NULL)
		{
			fprintf(stderr, "%s: %s" "\n", argv[0], "no memory");
			return EXIT_FAILURE;
		}
	}}

	/* Read input and get counts of different input patterns */
	{{
		int i, index, j;
		const char *filename, *errmsg;
		uint32_t mask;

		/* Loop over inputs */ 
		for (i = optind; i < argc; ++i)
		{
			/* Get track index */
			(void)parseinput(&filename, &mask, argv[i], i - optind);
			(void)parsetrackmask(&index, mask);

			/* Read input data data */
			j = ((index - 1) + (noncontrols + 1)) % (noncontrols + 1);
			errmsg = readdmrcounts(&counts[j * problem.counts_stride], opts.site_offset,
				&problem, filename);
			if (errmsg != NULL)
			{
				/* Free memory */
				free(handle);

				/* Print error */
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], filename, errmsg);
				return EXIT_FAILURE;
			}
		}
	}}

	/* Create input transformation tables */
	{{
		int i;

		/* Get the flags */
		for (i = 0; i < inputs; ++i)
			dmml_input_table(&flags[i * problem.flags_stride], problem.letters, problem.order, problem.tracks, trackmasks[i]);
	}}

	/* Optimize */
	{{
		unsigned long loop, iter;

		/* Initialize objective */
		best_logl = -HUGE_VAL;

		/* Loop over an appropriate number of resets */
		for (loop = 0; loop < opts.resets + 1; ++loop)
		{
			double parms[32 + 1], cumcost, logl, lastcumcost, lastlogl;

			/* Load initial parameters */
			{{ 
				int i;

				/* Error parameter */
				parms[0] = opts.error;

				/* Set tumor sample impurities */
				if (noncontrols >= 1)
					parms[1] = dmml_halton(loop, 0, 1. - opts.purity1);
				if (noncontrols >= 2)
					parms[2] = dmml_halton(loop, 1, 1. - opts.purity2);
				for (i = 2; i < noncontrols; ++i)
					parms[i+1] = dmml_halton(loop, i, .5);

				/* Set control impurity */
				if (has_control)
					parms[i+1] = 1.;
			}}

			/* Zero model table ~ uniformize latent state distribution */
			memset(models, 0, inputs * model_stride * sizeof(*models));

			/* Optimize */
			cumcost = HUGE_VAL;
			logl = -HUGE_VAL;
			for (iter = 0; iter < opts.max_iters; ++iter)
			{
				double obj[1 + 32 + (32+1)*32/2];

				/* Update model tables */
				update_models(models, model_stride, &problem, inputs, parms, trackmasks); 

				/* Zero approximating of the expected likelihood */
				memset(expect, 0, inputs * model_stride * sizeof(*expect));

				/* Rotate cost */
				lastcumcost = cumcost;
				cumcost = 0.;

				/* Update expected likelihood */
				{{ 
					size_t r;
					int i;

					for (r = 0; r < problem.rows; ++r)
					{
						/* Compute latent distribution */
						cumcost += get_latent(latent, &problem, inputs, 
							&counts[r * problem.matchcombs], problem.counts_stride, flags, problem.flags_stride,
								models, model_stride);

						/* Update */
						for (i = 0; i < inputs; ++i)
							dmml_expect_update(&expect[i * model_stride], 1, problem.cols, problem.matchcombs,
								latent, &flags[i * problem.flags_stride] , &counts[r * problem.matchcombs + i * problem.counts_stride] );
					}
				}}

				/* Check if the optimzer has stalled */
				{{
					/* Get log-likelihood */
					lastlogl = logl;
					logl = (cumcost - lastcumcost) / opts.order;
	
					/* Stop? */
					if (logl - lastlogl < opts.min_delta)
						break;
				}}

				/* Apply inputs */
				{{
					int i;

					/* Zero objective */
					memset(obj, 0, dmml_obj_size(noncontrols + 1) * sizeof(*obj));

					/* Process tumor sample data */
					for (i = 0; i < noncontrols; ++i)
					{
						/* Set up parameter indices */
						uint32_t inds[1];
						inds[0] = i + 1;

						/* Update objective */
						dmml_obj_update(obj, noncontrols + 1, opts.order, trackmasks[i],
							inds, 0, parms, &expect[i * model_stride]);
					}

					/* Process control */
					if (has_control)
					{
						/* Set up parameter indices */
						static const uint32_t inds[1] = { 0 };
						const int t = noncontrols; 

						/* Update objective */
						dmml_obj_update(obj, noncontrols + 1, opts.order, trackmasks[noncontrols],
							inds, 0, parms, &expect[t * model_stride]);
					}
				}}

				/* Add prior information */
				dmml_obj_prior(obj, noncontrols + 1, 0, parms[0], opts.error, opts.error_prec);
				dmml_obj_prior(obj, noncontrols + 1, 1, parms[1], 1. - opts.purity1, opts.purity1_prec);
				dmml_obj_prior(obj, noncontrols + 1, 2, parms[2], 1. - opts.purity2, opts.purity2_prec);

				/* Solve new parameters by optimizing the quadratic objective approximation */
				{{
					double scale, newobj[countof(obj)], newparms[countof(parms)];
					size_t j;

					/* NOTE: I've changed the line search to this incrementally
					 * regularizing solver, as the search direction might be ill-defined
					 * causing a stall, even though we migh have info on some parameters
					 */

					/* We loop towards the current set of parameters */
					for (scale = 1.; scale > 0.; scale *= .5)
					{
						/* Get regularized objective, no reg on 1st round */
						for (j = 0; j < countof(obj); ++j)
							newobj[j] = scale * obj[j];
						for (j = 0; j < (size_t)(noncontrols + 1); ++j)
							dmml_obj_prior(newobj, noncontrols + 1, j, parms[j], parms[j], 1. - scale);

						/* Solve */
						memcpy(newparms, parms, (noncontrols + 1) * sizeof(*parms));
						dmml_obj_solve(newparms, newobj, (noncontrols + 1));

						/* Check for bounds */
						for (j = 0; j < (size_t)(noncontrols + 1); ++j)
							if (!(0. < newparms[j] && newparms[j] < 1.))
								goto bad;

						/* Done? */
						break;
bad:;
					}

					/* Copy in the new solution */
					memcpy(parms, newparms, (noncontrols + 1) * sizeof(*parms));
				}}

			} /* for (iter) */

			/* Keep best solution stored */
			if (logl > best_logl)
			{
				/* Store params*/
				best_logl = logl;
				memcpy(best_parms, parms, (1 + noncontrols + has_control) * sizeof(*best_parms));
					/* NOTE: add has_control to bring over the dummy control purity parm as well */

				/* Store model table--we'll need it to recover the state */
				memcpy(best_lut, models, inputs * model_stride * sizeof(*models));
			}

		} /* for (loop) */
	}}

	/* Dump parameters if selected */
	if (opts.output_params != NULL)
	{
		static const char *const parmhead[] = { "nlogl", "error", "purity-left", "purity-right" };
		double parmdata[countof(parmhead)];
		tablewriter_t writer;
		int i, parmhead_count;

		/* Suppress output for tumor #2 if unused */
		parmhead_count = (int)countof(parmhead);
		if (!has_tumor2)
			--parmhead_count;

		/* Create a writer */
		tablewriter_init(&writer, opts.output_params,
			modestr( opts.output_params, opts.output_params_mode));

		/* Add column names */
		for (i = 0; i < parmhead_count; ++i)
			tablewriter_addcolumn(&writer, parmhead[i]);
		for (i = 2; i < noncontrols; ++i)
		{
			char colname[32];
			sprintf(colname, "purity-%lu", (unsigned long)trackmasks[i]);
			tablewriter_addcolumn(&writer, colname);
		}
		 
		/* Collect first few parameters */
		parmdata[0] = -best_logl;
		parmdata[1] = best_parms[0]; /* Error */
		parmdata[2] = 1. - best_parms[1]; 
		parmdata[3] = 1. - best_parms[2];

		/* Add data */
		tablewriter_put(&writer, parmdata, parmhead_count);

		for (i = 2; i < noncontrols; ++i)
		{
			double purity;
			purity = 1 - best_parms[i+1];
			tablewriter_put(&writer, &purity, 1);
		}

		/* Close file */
		if (tablewriter_deinit(&writer) != 0)
			/* NOTE: don't exit--we'd lose all results */
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.output_params, "write error");
	}

	/* Dump latent? */
	if (opts.output_latent != NULL)
	{
		tablewriter_t writer;
		size_t s, r;

		/* Open writer */
		tablewriter_init(&writer, opts.output_latent,
			modestr(opts.output_latent, opts.output_latent_mode));

		/* Write header */
		for (s = 0; s < problem.cols; ++s)
			/* NOTE: don't bother to write the header, it gets really long easily */
			tablewriter_addcolumn(&writer, ".");

		/* Dump data : warning --these are log-probs */
		for (r = 0; r < problem.rows; ++r)
		{
			/* Summon back a column */
			get_latent(latent, &problem, inputs, 
				&counts[r * problem.matchcombs], problem.counts_stride, flags, problem.flags_stride,
					best_lut, model_stride);

			/* Write */
			tablewriter_put(&writer, latent, problem.cols);
		}

		/* Close */
		if (tablewriter_deinit(&writer) != 0)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.output_latent, "write error");
	}

	/* Dump mehylome distributions? */
	if (opts.output_probs != NULL)
	{
		tablewriter_t writer;
		int r;
		size_t marginal_cols, s, c;

		/* Open writer */
		tablewriter_init(&writer, opts.output_probs,
			modestr(opts.output_probs, opts.output_probs_mode));

		/* Suppress output for tumor #2 if unused */
		marginal_cols = dmml_marginal_cols(problem.letters, problem.tracks);
		if (!has_tumor2)
			marginal_cols /= 2;

		/* Emit column names */
		{{
			for (c = 0; c < marginal_cols; ++c)
				if (c < 8) /* NOTE: 8 is historic */
				{
					char colname[32], *s;
					unsigned d0, d1, d2;

					/* Get digits */
					d0 = (c / 1u) % problem.letters;
					d1 = (c / problem.letters) % problem.letters;
					d2 = (c / (problem.letters * problem.letters)) % problem.letters;

					/* All tracks covered? */
					s = "";
					if (noncontrols > 2)
						s = "...";

					/* Format & add column */
					if (!has_tumor2)
						sprintf(colname, "(z0,z1%s)=(%u,%u%s)", s, d0, d1, s);
					else
						sprintf(colname, "(z0,z1,z2%s)=(%u,%u,%u%s)", s, d0, d1, d2, s);
					tablewriter_addcolumn(&writer, colname);
				}
				else
					tablewriter_addcolumn(&writer, ".");
		}}

		/* Summon the final model */
		get_models(models, model_stride, &problem, inputs, best_parms, trackmasks);

		/* Pull few first cols */
		for (r = 0; r < problem.order - 1; ++r)
			get_latent(&latent[(r + 1) * problem.cols], &problem, inputs, 
				&counts[r * problem.matchcombs], problem.counts_stride, flags, problem.flags_stride,
					models, model_stride);
		 
		/* Loop over the sites */
		for (s = 0; s < problem.sites; ++s)
		{
			double psum;

			/* Rotate first column out of the window */
			memmove(latent, &latent[1 * problem.cols], (problem.order - 1) * problem.cols * sizeof(*latent));

			/* Pull in the next column */
			get_latent(&latent[(problem.order - 1) * problem.cols], &problem, inputs, 
				&counts[(s + (problem.order - 1)) * problem.matchcombs], problem.counts_stride, flags, problem.flags_stride,
					models, model_stride);

			/* Loop over the output columns */
			for (c = 0; c < marginal_cols; ++c)
			{
				/* Get element */
				psum = exp(dmml_marginal_element(problem.letters, problem.order, problem.tracks,
					&latent[-s * problem.cols], s, c)); /* TODO: <-- UB */
				if (!has_tumor2)
					psum += exp(dmml_marginal_element(problem.letters, problem.order, problem.tracks,
						&latent[-s * problem.cols], s, c | 4u));

				/* Write */
				tablewriter_put(&writer, &psum, 1);
			}
		}

		/* Clean up */
		if (tablewriter_deinit(&writer) != 0)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.output_probs, "write error");
	}

	/* Dump p-values */
	if (opts.output_pvalues != NULL)
	{
		tablewriter_t writer;
		int r;
		size_t s;

		/* Open writers */
		if (opts.output_pvalues)
			tablewriter_init(&writer, opts.output_pvalues,
				modestr(opts.output_pvalues, opts.output_pvalues_mode));

		/* Put rest of the headers */
		int i, ii;
		static const char *const tracknames[] = { "control", "left", "right" };

		/* Loop over pairs of tumor smaples */
		for (i = 0; i < noncontrols + 1; ++i)
			for (ii = i+1; ii < noncontrols + 1; ++ii)
				if (!(!has_tumor2 && ii == 2))
				{
					char colname[32], *p;

					/* Make column label */
					p = colname;
					{{
						p += sprintf(colname, "p-value ");
					}}
					if (i < (int)countof(tracknames))
						p += sprintf(p, "%s", tracknames[i]);
					else
						p += sprintf(p, "%lu",
							(unsigned long)trackmasks[(unsigned)(i + noncontrols) % (unsigned)(noncontrols + 1)]);
					{{
						p += sprintf(p, " vs ");
					}}
					if (ii < (int)countof(tracknames))
						p += sprintf(p, "%s", tracknames[ii]);
					else
						p += sprintf(p, "%lu",
						(unsigned long)trackmasks[(unsigned)(ii + noncontrols) % (unsigned)(noncontrols + 1)]);

					/* Add column */
					tablewriter_addcolumn(&writer, colname );
				}

		/* Summon the final models */
		get_models(models, model_stride, &problem, inputs, best_parms, trackmasks);

		/* Pull in first few of the columns */
		for (r = 0; r < problem.order - 1; ++r)
			get_unscaled_latent(&latent[(r + 1) * problem.cols], &problem, inputs, 
				&counts[r * problem.matchcombs], problem.counts_stride, flags, problem.flags_stride,
					models, model_stride);

		/* Loop over the sites */
		for (s = 0; s < problem.sites; ++s)
		{
			/* Rotate first column out of the window */
			memmove(latent, &latent[1 * problem.cols], (problem.order - 1) * problem.cols * sizeof(*latent));

			/* Pull in the next column */
			get_unscaled_latent(&latent[(problem.order - 1) * problem.cols], &problem, inputs, 
				&counts[(s + (problem.order - 1)) * problem.matchcombs], problem.counts_stride, flags, problem.flags_stride,
					models, model_stride);

			/* GEt p-values */
			{{
				size_t c;
				int i, ii;

				/* Loop over pairs of tumor samples */
				for (i = 0; i < noncontrols + 1; ++i)
					for (ii = i+1; ii < noncontrols + 1; ++ii)
						if (!(!has_tumor2 && ii == 2))
						{
						size_t sa, sb; /* Shifts for i:th and ii:th track */
						double l0, l1; /* Null, alt likelihood */
						double lpsum, lrstat, pv;

						/* Get shifts */
						sa = ipowl(problem.letters, i);
						sb = ipowl(problem.letters, ii);

						/* Compute likelihoods for the null and alternative models */
						l0 = l1 = log(0.);
						for (c = 0; c < dmml_marginal_cols(problem.letters, problem.tracks); ++c)
						{
							/* Get */
							lpsum = dmml_marginal_element(problem.letters, problem.order, problem.tracks,
								&latent[-s * problem.cols], s, c); /* TODO: <-- UB */

							/* This columns is in the null model? */
							if ((c / sa) % problem.letters == (c / sb) % problem.letters)
								l0 = logaddexp(l0, lpsum);

							/* This column is in the alternative model */
							l1 = logaddexp(l1, lpsum);
						}

						/* Compute test statistic & the p-value */
						lrstat = -2. * ( l0 - l1 );
						pv = dmml_chi2cdf(lrstat, problem.letters - 1, 1);

						/* Write p-value */
						tablewriter_put(&writer, &pv, 1);
						}
			}}
		}

		/* Close */
		if ( tablewriter_deinit(&writer) != 0)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.output_pvalues, "write error");
	}

	/* Free memory */
	free(handle);

	return EXIT_SUCCESS;
}
