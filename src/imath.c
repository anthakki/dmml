
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "imath.h"
#include <assert.h>

unsigned long
ipowl(unsigned long x, unsigned long y)
{
	unsigned long r;

	/* Loop */
	r = 1;
	for (; y > 0; y /= 2)
	{
		/* Factor power in the result */
		if ((y & 1lu) != 0)
			r *= x;
		/* Square */
		x *= x;
	}

	return r;
}

int
popcntl(unsigned long x)
{
	int count;

	/* Compute number of set bits */
	count = 0;
	for (; x > 0; x &= (x-1))
		++count;

	return count;
}
