#ifndef DMRB_H_
#define DMRB_H_

/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <stddef.h>
#include <stdint.h>

/*
 * A reader for .dmrb files. These are binary files, which contains the read
 * data grouped by read and sorted by site.
 */

#ifdef __cplusplus
extern "C" {
#endif

/* Type for a .dmrb reader */
typedef struct dmrb_reader_ dmrb_reader_t;

/* Record type */
typedef struct dmrb_record_ {
	uint32_t site, read; /* Site, read indices */
	uint64_t value;      /* Value */
} dmrb_record_t;


/* Create a new reader */
int dmrb_reader_init(dmrb_reader_t *self, const char *filename);

/* Get record limits */
const dmrb_record_t *dmrb_reader_limits(const dmrb_reader_t *self);

/* Rewind the reader */
int dmrb_reader_rewind(dmrb_reader_t *self);

/* Extract record(s) */
int dmrb_reader_read(dmrb_reader_t *self, dmrb_record_t *records, size_t count);

/* Check if we're successfully at the end */
int dmrb_reader_eof(const dmrb_reader_t *self);

/* Destroy a reader */
void dmrb_reader_deinit(dmrb_reader_t *self);


/*- Guts-- don't look! */

struct dmrb_reader_ {
	void *file_;
	dmrb_record_t limits_;
};

#ifdef __cplusplus
}
#endif

#endif /* DMRB_H_ */
