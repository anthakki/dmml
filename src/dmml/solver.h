#ifndef DMML_SOLVER_H_
#define DMML_SOLVER_H_

/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * The objective is a 1 + dim + (dim+1)*dim/2 array which contains the
 * quadratic approximation of the objective where dim is the number of free
 * parameters.
 */

/* Compute size of the objective array */
size_t dmml_obj_size(uint32_t dim);

/* Add prior information about the ind:th parameter; parm is the current
  parameter value, and mean and prec are the prior mean and precision */
void dmml_obj_prior(double *obj, uint32_t dim, uint32_t ind, double parm, double mean, double prec);

/* Add information from the expected likelihood */ 
void dmml_obj_update(double *obj, uint32_t dim, uint8_t order, uint32_t trackmask,
	const uint32_t *alphainds, uint32_t errind, const double *parms, const double *expect);

/* Maximize objective */
void dmml_obj_solve(double *parms, const double *obj, uint32_t dim);

/* Scale parameters back inside the limits [0,1] */
void dmml_parms_backout(double *parms, const double *oldparms, uint32_t dim);

#ifdef __cplusplus
}
#endif

#endif /* DMML_SOLVER_H_ */
