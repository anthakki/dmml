#ifndef DMML_MODEL_H_
#define DMML_MODEL_H_

/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * The input table maps is a letters**(tracks*order) by (letters+1)**order
 * table, which maps input characters to model table indices for each
 * internal state variable.
 */

/* Computes the width of the input table */
size_t dmml_input_size(uint8_t letters, uint8_t order);

/* Computes a lookup table for translating the inputs; inputtab must be
  dmml_latent_cols() * dmml_input_size() and need not to be initialized */
void dmml_input_table(uint32_t *inputtab, uint8_t letters, uint8_t order, uint8_t tracks, uint32_t trackmask);

/*
 * The model table is (2**n+1)**order element table contaning the distinct
 * model probabilities under the current parameters, where n is the number
 * tracks used in this input type, as specified by trackmask.
 */

/* Computes the size of the model table */
size_t dmml_model_size(uint32_t order, uint32_t trackmask);

/* Updates the lookup table for model probabilities
  modeltab must be dmml_model_table_size() and be pre-zeroed */
void dmml_model_table(double *modeltab, uint32_t order, uint32_t trackmask, const double *alphas, double err);

/*
 * The latent matrix is a sites+(order-1) by letters**(tracks*order) element
 * array, which contains the distributions of the latent state variables at the
 * different sites.
 */

/* Computes the height of the latent array */
size_t dmml_latent_rows(size_t sites, uint8_t order);
/* Computes the width of the latent array */
size_t dmml_latent_cols(uint8_t letters, uint8_t order, uint8_t tracks);

/* Accumulates likelihood for the latent variable distribution; latent must be
  rows*cols and pre-zeroed; the array can be processed row at a time */
void dmml_latent_update(double *latent, size_t rows, size_t cols, size_t inputs,
	const uint32_t *inputtab, const double *modeltab, const uint32_t *counts);

/* Scales the latent variable distribution; returns log-likelihood */ 
double dmml_latent_scale(double *latent, size_t rows, uint32_t cols);

/*
 * The expect is a (2**n+1)**order table (same size as modeltab) contaning the
 * decomposition of the expected likelihood function.
 */

/* Accumulates the expected model likelihood; assumptions as above  */
void dmml_expect_update(double *expect, size_t rows, size_t cols, size_t inputs,
	const double *latent, const uint32_t *inputtab, const uint32_t *counts);

/*
 * The following routines compute the marginal probability at a single
 * site/column; the value is encoded in the column index:
 *   z0*letters^0 + z1*letters^1 + ...
 */

/* Get number of marginal columns */
size_t dmml_marginal_cols(uint8_t letters, uint8_t tracks);

/* Computes the marginal probability of the joint methylation patterns 
  at a single site for a given value (col) */ 
double dmml_marginal_element(uint8_t letters, uint8_t order, uint8_t tracks,
	const double *latent, size_t site, uint32_t col);

#ifdef __cplusplus
}
#endif

#endif /* DMML_MODEL_H_ */
