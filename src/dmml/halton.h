#ifndef DMML_HALTON_H_
#define DMML_HALTON_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Returns the index:th Halton number of a given base */
double dmml_haltonnumber(unsigned long index, unsigned long base);

/* Generates a coordinate of the index:th point of a Halton
  sequence starting at seed */
double dmml_halton(unsigned long index, unsigned long dim, double seed);

#ifdef __cplusplus
}
#endif

#endif /* DMML_HALTON_H_ */
