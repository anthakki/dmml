#ifndef DMML_CHI2CDF_H_
#define DMML_CHI2CDF_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Cumulative density of a chi-squared distribution with df
  degrees of freedom. Tail can be -1/+1 for left/right */
double dmml_chi2cdf(double x, unsigned long df, int tail);

#ifdef __cplusplus
}
#endif

#endif /* DMML_CHI2CDF_H_ */
