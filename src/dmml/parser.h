#ifndef DMML_PARSER_H_
#define DMML_PARSER_H_

/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* 
 * The parser maintains a sliding window of inputs to help parsing sequences of
 * (site, letter) pairs into input combinations, which is useful for
 * higher-order models.
 */

/* An opaque parser type */
typedef struct dmml_parser_ dmml_parser_t;

/* Create a new parser */
void dmml_parser_init(dmml_parser_t *parser, uint8_t letters, uint8_t order);

/* Get parser location */
uint32_t dmml_parser_tell(const dmml_parser_t *parser);
/* Set parser location */
void dmml_parser_seek(dmml_parser_t *parser, uint32_t site);

/*
 * The combination index in below is as follows:
 *   comb := sum( b^(k-1) * d(k) , k=1..order )
 * where b = letters + 1 is the base of the extended alphabet, and d(k) is the
 * k:th digit, d(1) representing the first item in the sliding window with
 * d(k) = 0 if the input is undefined and d(k) = l+1 for the l:th letter.
 */

/* Push in not-a-letter(s); returns the index of the current combination */
uint32_t dmml_parser_skip(dmml_parser_t *parser, uint32_t count);
/* Push a new letter into the parser */
uint32_t dmml_parser_put(dmml_parser_t *parser, uint8_t letter);

/*
 * The below functions offer a higher-level interface, which can be be used to
 * feed site indices if the data are first grouped by read index and then
 * sorted by the site index. A single letter may emit multiple combinations, as
 * the samples need to be shifted out of the window as well.
 */

/* Feed a letter into the process; returns -1 if the site is invalid */
int dmml_parser_push(dmml_parser_t *parser, uint32_t *counts, uint32_t site_offset, uint32_t sites, int read_edge, uint32_t site, uint8_t letter);
/* Flush the window after the last read */
void dmml_parser_flush(dmml_parser_t *parser, uint32_t *counts, uint32_t site_offset, uint32_t sites);

/*- Guts--don't look */

struct dmml_parser_ {
	uint8_t letters_, order_;
	uint32_t base_, shift_;
	uint32_t site_, comb_;
};

#ifdef __cplusplus
}
#endif

#endif /* DMML_PARSER_H_ */
