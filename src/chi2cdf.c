
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmml/chi2cdf.h"
#include <assert.h>
#include <math.h>
#include <stddef.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
double
qgamma_rec(unsigned long ds, double y, double lg, double s, double x)
{
	double sum, t1, t2;

	/* Computes Gamma(s+sd,x)/Gamma(s+sd) via the recurrence:
	  Gamma(s+1,x) = s*Gamma(s,x) + x^s*exp(-x) */
	sum = 0.;
	while (ds-- > 0)
		sum = x/(s+ds) * (1+sum);

	/* Handle NaNs */
	t1 = (s-1)*log(x)-x;
	if (!(t1 == t1))
		t1 = -x;
	t2 = exp( t1-lg ) * sum;
	if (!(t2 == t2))
		t2 = exp( t1-lg );

  return y + t2;
}

static
double
local_erfc(double x)
{
	static const double coefs[] = { -1.26551223, 1.00002368, 0.37409196,
		0.09678418, -0.18628806, 0.27886807, -1.13520398, 1.48851587,
		-0.82215223, 0.17087277 };

	double t, tt, p, tau;
	size_t i;

	/* Compute "t" */
	t = 1/(1 + .5*fabs(x));

	/* Expand polynomial on "t" */
	p = coefs[0];
	tt = 1.;
	for (i = 1; i < countof(coefs); ++i)
	{
		tt *= t;
		p += coefs[i] * tt;
	}

	/* Get "tau" */
	tau = t * exp(-x*x + p);

	return x < 0. ? -tau : tau;
}

#define HALF_LOG_PI (.5*log(4*atan(1.)))

double
dmml_chi2cdf(double x, unsigned long df, int tail)
{
	double upper;

	/* Compute mass in the upper tail */
	if (x <= 0.) /* NOTE: NaNs take the long path */
		upper = 1.;
	else
		switch (df % 2)
		{
			case 1: /* odd */
				upper = qgamma_rec( (df-1)/2, local_erfc(sqrt(.5*x)), HALF_LOG_PI, .5, .5*x );
				break;
			default: /* even */
				upper = qgamma_rec( (df-1)/2, exp(-.5*x), 0., 1., .5*x );
				break;
		}

	/* Return correct tail */
	switch ((tail > 0) - (tail < 0))
	{
		case -1:
			return 1. - upper;
		case +1:
			return upper;
		default:
			return 1.;
	}
}
