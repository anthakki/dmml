
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmml/model.h"
#include "imath.h"
#include <assert.h>
#include <math.h>

size_t
dmml_input_size(uint8_t letters, uint8_t order)
{
	assert(letters > 0);
	assert(order > 0);

	/* TODO: overflow check */

	return ipowl(letters + 1, order);
}

void
dmml_input_table(uint32_t *inputtab, uint8_t letters, uint8_t order, uint8_t tracks, uint32_t trackmask)
{
	uint32_t cmpcombs, lettersp1, rows, cols, trackshift, j, i, flag, flagshift, ileft, jshift, k, idigit, jleft, vref, bitmap, s, m, v, jdigit, letter;

	assert(inputtab != NULL);
	assert(letters > 0);
	assert(order > 0);
	assert(tracks > 0);

	/* TODO: overflow check in assert */

	/* NOTE: the hardcoded base-2/bitshift here implies that we track
	  matches/mismatches; could change in the future */

	/* Combinations at comparison */
	cmpcombs = ipowl(2, popcntl(trackmask)) + 1;
	lettersp1 = letters + 1;

	/* Compute lookup table dimension */
	rows = ipowl(lettersp1, order);
	cols = ipowl(letters, (uint32_t)tracks * order);

	/* Base of digits for a single track */
	trackshift = ipowl(letters, order);

	/* Loop */
	for (j = 0; j < cols; ++j) /* TODO: Transposed table--rename? */
		for (i = 0; i < rows; ++i)
		{
			/* Flag is a lookup index to the model table 
			  flag is in [0, cmpcombs**order) */
			flag = 0;
			flagshift = 1;

			/* Bits left from i */
			ileft = i;

			/* Shift for j */
			jshift = 1;

			/* Process */
			for (k = 0; k < order; ++k)
			{
				/* Pop digit */
				idigit = ileft % lettersp1; /* idigit is 0 for NA and l+1 for the l:th letter */
				ileft /= lettersp1;

				/* Input is not NaN */
				if (idigit > 0)
				{
					/* Bits left from j */
					jleft = j / jshift;

					/* Build bitmap of matching tracks */
					vref = 1;
					bitmap = 0;
					s = 0;
					for (m = trackmask; m > 0; m &= (m-1))
					{
						/* Mask for this track */
						v = m & ~(m-1);

						/* Pop digits to shift in the correct track */
						for (; v > vref; vref <<= 1)
							jleft /= trackshift;

						/* Compare */
						jdigit = jleft % letters;
						if (jdigit == idigit - 1)
							bitmap |= (1lu << s);

						/* Shift output pointer */
						++s;
					}

					/* Bump bitmap to letter; NA is 0 */
					letter = bitmap + 1;
				}
				else
					/* NA maps to NA */
					letter = 0;
				
				/* Push digit & shift */
				flag += flagshift * letter;
				flagshift *= cmpcombs;

				/* Move shift */
				jshift *= letters;
			}

			/* Store flag */
			inputtab[j*rows + i] = flag;
		}
}

size_t
dmml_model_size(uint32_t order, uint32_t trackmask)
{
	assert(order > 0);

	/* TODO: check overflow */

	/* Compute lookup table size */
	return ipowl(ipowl(2, popcntl(trackmask)) + 1, order);
}

void
dmml_model_table(double *modeltab, uint32_t order, uint32_t trackmask, const double *alphas, double err)
{
	uint32_t cmpcombs, inputcombs, i, s, m, ileft, idigit, match, w;
	double matchprobs[2], prob, term;

	assert(modeltab != NULL);
	assert(order > 0);

	/* TODO: overflow check in assert */

	/* NOTE: modeltab is assumed to be zeroed !!! */

	/* Combinations at comparison */
	cmpcombs = ipowl(2, popcntl(trackmask)) + 1;
	inputcombs = ipowl(cmpcombs, order);

	/* Put match/no match probabilities in a table */
	matchprobs[0] = err;
	matchprobs[1] = 1. - err;

	/* Loop */
	for (i = 0; i < inputcombs; ++i)
	{
		/* The result goes here */
		prob = 0.;

		/* Loop throuhg tracks */
		s = 0;
		for (m = trackmask; m > 0; m &= (m-1))
		{
			/* Input bits left to process */
			ileft = i;

			/* Loop through positions */
			term = 1.;
			for (w = 0; w < order; ++w)
			{
				/* Get digit */
				idigit = ileft % cmpcombs;
				ileft /= cmpcombs;

				if (idigit > 0)
				{
					/* Get match/no mach bit */
					match = ((idigit - 1) >> s) & 1u;
					/* Factor in flip/no flip probablity */
					term *= matchprobs[match];
				}
				else
					/* Factor in unity */
				{}
			}

			/* Accumulate */
			prob += alphas[s] * term;

			/* Update shift */
			++s;
		}

		/* Store log-odds in the table */
		modeltab[i] += log(prob);
	}
}

size_t
dmml_latent_rows(size_t sites, uint8_t order)
{
	assert(order > 0);

	/* TODO: check overflow */

	/* Compute number of rows */
	return sites + (order - 1);
}

size_t
dmml_latent_cols(uint8_t letters, uint8_t order, uint8_t tracks)
{
	assert(letters > 0);
	assert(order > 0);
	assert(tracks > 0);

	/* TODO: check overflow */

	/* Compute number of columns */
	return ipowl(letters, tracks * order);
}

void
dmml_latent_update(double *latent, size_t rows, size_t cols, size_t inputs, const uint32_t *inputtab, const double *modeltab, const uint32_t *counts)
{
	size_t i, j, l;
	double sum;

	/* Loop */
	for (i = 0; i < rows; ++i)
		for (j = 0; j < cols; ++j)
		{
			/* Compute dot */
			sum = 0.;
			for (l = 0 ; l < inputs; ++l)
				sum += modeltab[inputtab[j*inputs + l]] * counts[i*inputs + l];

			/* Record */
			latent[i*cols + j] += sum;
		}
}

double
dmml_latent_scale(double *latent, size_t rows, uint32_t cols)
{
	double logl, *row, major, expminor, offset;
	size_t i;
	uint32_t j;

	assert(latent != NULL);
	assert(cols > 0);

	/* Zero log-likelihood */
	logl = 0.;

	/* Loop through rows */
	for (i = 0; i < rows; ++i)
	{
		/* Get row pointer */
		row = &latent[i * cols];

		/* NOTE: two pass computation to avoid loss of precision */

		/* Find major term */
		major = row[0];
		for (j = 1; j < cols; ++j)
			if (row[j] > major)
				major = row[j];

		/* Find correction term */
		expminor = 0.;
		for (j = 0; j < cols; ++j)
			expminor += exp(row[j] - major);

		/* Compute normalization term */
		offset = -(major + log(expminor));
		logl += -offset;

		/* Normalize row */
		for (j = 0; j < cols; ++j)
			row[j] += offset;
	}

	return logl;
}

void
dmml_expect_update(double *expect, size_t rows, size_t cols, size_t inputs, const double *latent, const uint32_t *inputtab, const uint32_t *counts) 
{
	size_t i, j, l;
	double explat;

	/* Loop through the sites */
	for (i = 0; i < rows; ++i)
		for (j = 0; j < cols; ++j)
		{
			/* Get the latent probability */
			explat = exp(latent[i*cols + j]);

			/* Loop through possible inputs */
			for (l = 0; l < inputs; ++l)
				expect[inputtab[j*inputs + l]] += counts[i*inputs + l] * explat;
		}
}

size_t
dmml_marginal_cols(uint8_t letters, uint8_t tracks)
{
	assert(letters > 0);
	assert(tracks > 0);

	/* Get marginal distribution width */
	return ipowl(letters, tracks);
}

static
double
logaddexp(double x, double y)
{
	/* Computes log(exp(x) + exp(y)) but avoids underflow */
	if (x < y)
		return y + log(1. + exp(x - y));
	else
		return x + log(1. + exp(y - x));
}

double
dmml_marginal_element(uint8_t letters, uint8_t order, uint8_t tracks, const double *latent, size_t site, uint32_t col)
{
	uint32_t dststride, srcstride, sources, trackwidth, shift, k, srcrow, j, x1, x2, y, ys, t;
	double s;

	/* Get width of source & destination arrays */
	dststride = ipowl(letters, tracks);
	srcstride = ipowl(letters, tracks * order);

	/* How many source elements we pack per a single destination element? */
	sources = srcstride / dststride;

	/* Loop */
	trackwidth = ipowl(letters, order);
	shift = trackwidth;
	s = log(0.);
	for (k = 0; k < order; ++k)
	{
		/* Source row */
		srcrow = site + k;

		/* Update current shift */
		shift /= letters;

		for (j = 0; j < sources; ++j)
		{
			/* Inputs */
			x1 = j;
			x2 = col;

			/* output */
			y = 0;
			ys = 1;

			/* Loop */
			for (t = 0; t < tracks; ++t)
			{
				/* Pull order-(k+1) low digits from x1 */
				y += (x1 % shift) * ys;
				x1 /= shift;
				ys *= shift;

				/* Pull 1 digit from x2 */
				y += (x2 % letters) * ys;
				x2 /= letters;
				ys *= letters;

				/* Pull k digits more from x1 */
				y += (x1 % (trackwidth / shift / letters)) * ys;
				x1 /= (trackwidth / shift / letters);
				ys *= (trackwidth / shift / letters);
			}

			/* Accumulate */
			s = logaddexp(s, latent[srcrow*srcstride + y]);
		}
	}

	return s - log(order);
}
