
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmml/solver.h"
#include "imath.h"
#include <assert.h>
#include <math.h>

/* This macro computes the index for the (i,j):th element of the 
	the Hessian for i >= j */
#define HESSI(d, i, j) \
	( (i) + (2*(d)-(j)-1)*(j)/2 )

size_t
dmml_obj_size(uint32_t dim)
{
	/* NOTE: We need space for the object, gradient, and lower triangle of the
		Hessian */
	return 1 + dim + (dim+1)*dim/2;
}

void
dmml_obj_prior(double *obj, uint32_t dim, uint32_t ind, double parm, double mean, double prec)
{
	double *grad, *hess;

	assert(obj != NULL || dim < 1);
	assert(ind < dim);

	/* Update gradient */
	grad = &obj[1];
	grad[ind] += prec * ( mean/parm - (1.-mean)/(1.-parm) );

	/* Update Hessian */
	hess = &grad[dim];
	hess[HESSI(dim, ind, ind)] += -prec * ( mean/(parm*parm) + (1.-mean)/((1.-parm)*(1.-parm)) );
}

void
dmml_obj_update(double *obj, uint32_t dim, uint8_t order, uint32_t trackmask, const uint32_t *alphainds, uint32_t errind, const double *parms, const double *expect)
{
	int trackcnt, match;
	uint32_t cmpcombs, inputcombs, i, s, m, ileft, k, idigit;
	double matchprobs[2], alphas[32], sum, *grad, *hess, prob, prob_e, prob_ee, terms[32], terms_e[32], term_ee, b, logprob_e, logterm_a;

	/* NOTE: this routine is similar to dmml_model_table() */

	/* Get track count */
	trackcnt = popcntl(trackmask);

	/* Stop here if no tracks */
	if (trackcnt < 1)
		return;

	/* TODO: check for overflows */

	/* Combinations at comparison */
	cmpcombs = ipowl(2, (uint32_t)trackcnt) + 1;
	inputcombs = ipowl(cmpcombs, order );

	/* Match/no match probabilities */
	matchprobs[0] = parms[errind];
	matchprobs[1] = 1. - parms[errind];

	/* Mixing parameters */
	{
		/* Copy free parameters */
		sum = 0.;
		for (i = 0; i < (uint32_t)(trackcnt - 1); ++i)
		{
			alphas[i] = parms[alphainds[i]];
			sum += alphas[i];
		}

		/* Get last */
		alphas[trackcnt-1] = 1. - sum;
	}

	/* NOTE: !!! obj is assumed to be zeroed !!! */

	/* Get gradient & Hessian pointers */
	grad = &obj[1];
	hess = &grad[dim];

	/* Loop */
	for (i = 0; i < inputcombs; ++i)
	{
		/* Variables to hold the objective and derivatives wrt the error parameter */
		prob = 0.;
		prob_e = prob_ee = 0.;

		/* Loop throuhg tracks */
		s = 0;
		for (m = trackmask; m > 0; m &= (m-1))
		{
			/* Bits to process */
			ileft = i;

			/* Hold current term and derivatives wrt the error */
			terms[s] = 1.;
			terms_e[s] = term_ee = 0.;  /* first, second derivative wrt err */

			/* Loop through positions */
			for (k = 0; k < order; ++k)
			{
				/* Get digit */
				idigit = ileft % cmpcombs;
				ileft /= cmpcombs;

				if (idigit > 0)
				{
					/* Get match/no mach bit */
					match = ((idigit - 1) >> s) & 1u;

					/*
					 * Update derivatives: we have t <- t * g
					 * where g = a+b*x  with  a=0, b=+1 for match=0
					 *                  and   a=1, b=-1 for match=1
					 * Now:
					 *   t' <- t'*g + t*g' = t'*g + t*b
					 * and:
					 *   t'' <- t''*g + 2*t'*g' + t*g'' = t''*g + 2*t'*b
					 *
					 * Update the stuff in reverse as te depends on old t etc.
					 */
					b = match ? -1. : +1.;
					term_ee = term_ee * matchprobs[match] + 2*b * terms_e[s];
					terms_e[s] = terms_e[s] * matchprobs[match] + b * terms[s];

					/* Factor in match/no match probablity */
					terms[s] *= matchprobs[match];
				}
				else
					/* Factor in 1. */
				{}
			}

			/* Accumulate objective */
			prob += alphas[s] * terms[s];

			/* Accumulate objective derivatives */
			prob_e += alphas[s] * terms_e[s];
			prob_ee += alphas[s] * term_ee;

			/* Update shift */
			++s;
		}

		/* Update objective */
		obj[0] += expect[i] * log(prob);

		/* Update derivatives wrt the error parameter */
		logprob_e = prob_e / prob;
		grad[errind] += expect[i] * prob_e / prob;
		hess[HESSI(dim, errind, errind)] += expect[i] * (prob_ee - prob_e * logprob_e) / prob;

		/* Update derivatives wrt the mixing parameters */
		for (s = 0; s < (uint32_t)(trackcnt - 1); ++s)
		{
			/* First & second derivative wrt to the mixing parameter */
			logterm_a = (terms[s] - terms[trackcnt-1]) / prob;
			grad[ alphainds[s] ] += expect[i] * logterm_a;
			hess[HESSI(dim, alphainds[s], alphainds[s])] += expect[i] * -(logterm_a * logterm_a);

			/* Second derivative wrt err, alphas[s] */
			hess[alphainds[s] >= errind ? HESSI(dim, alphainds[s], errind) :
				HESSI(dim, errind, alphainds[s])] += expect[i] * ((terms_e[s] - terms_e[trackcnt-1] - logterm_a * prob_e) / prob);

			/* NOTE: Derivatives wrt alphas[s], alphas[t] for s != t are zero */
		}
	}
}

void
dmml_obj_solve(double *parms, const double *obj, uint32_t dim)
{
	const double *grad, *hess;
	double h11, g1, coef, y1;
	uint32_t i;

	/* Early exit if empty */
	if (dim < 1)
		return;

	/* NOTE: Out Hessian has a nice structure, so we can solve it using a simple
		linear time algorithm. This is esseantially like using a LU decomposition */

	/* Get pointers */
	grad = &obj[1];
	hess = &grad[dim];

	/* Reduce Hessian to lower triangular form */
	h11 = hess[0];
	g1 = grad[0];
	for (i = 1; i < dim; ++i)
	{
		/* Eliminate value at H(0,i) */
		coef = hess[HESSI(dim, i, 0)] / hess[HESSI(dim, i, i)];
		h11 -= coef * hess[HESSI(dim, i, 0)];

		/* Apply same transformation on the gradient */
		g1 -= coef * grad[i];
	}

	/* Forward substitute solution */
	y1 = g1 / h11;
	parms[0] -= y1;
	for (i = 1; i < dim; ++i)
		parms[i] -= ( grad[i] - hess[HESSI(dim, i, 0)]*y1 ) / hess[HESSI(dim, i, i)];
}

void
dmml_parms_backout(double *parms, const double *oldparms, uint32_t dim)
{
	double scale, parm;
	uint32_t i;

	assert((parms != NULL || dim < 1));
	assert((oldparms != NULL || dim < 1));

	/* Find scale */
	scale = 1.;
	for (i = 0; i < dim; ++i)
		for (; scale > 0; scale *= .5)
		{
			parm = scale * parms[i] + (1. - scale) * oldparms[i];
			if (0. < parm && parm < 1.)
				break;
		}

	/* Scale back the parameters */
	for (i = 0; i < dim; ++i)
		parms[i] = scale * parms[i] + (1. - scale) * oldparms[i];
}
