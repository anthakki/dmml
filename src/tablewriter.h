#ifndef SRC_TABLEWRITER_H_
#define SRC_TABLEWRITER_H_

/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <stddef.h>

/*
 * A simple writer for dumping tables of floating point numbers into files
 * in binary/ascii (tab separated) formats.
 */

#ifdef __cplusplus
extern "C" {
#endif

/* Type for a writer */
typedef struct tablewriter_ tablewriter_t;

/* Creates a new writer; format can be "b" for binary and "z" for gzip compression;
 * returns 0 on success and -1 on error, but the writer can be still used */
int tablewriter_init(tablewriter_t *self, const char *filename, const char *format); 

/* Adds a column; columns must be added first such that the data shapes well */
int tablewriter_addcolumn(tablewriter_t *self, const char *colname);

/* Dumps a set of elements */
int tablewriter_put(tablewriter_t *self, const double *values, size_t count);

/* Destroys a writer; returns -1 if there were any errors */
int tablewriter_deinit(tablewriter_t *self);

/*- Guts--don't look */

struct tablewriter_ {
	void *file_;
	const struct tablewriter_methods_ *methods_;
	size_t col_, cols_;
	const char *sep_;
};

#ifdef __cplusplus
}
#endif

#endif /* SRC_TABLEWRITER_H_ */
