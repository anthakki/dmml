#ifndef SRC_IMATH_H_
#define SRC_IMATH_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Integer power of a long */
unsigned long ipowl(unsigned long x, unsigned long y);

/* Compute Hamming weight of a long */
int popcntl(unsigned long x);

#ifdef __cplusplus
}
#endif

#endif /* SRC_IMATH_H_ */
