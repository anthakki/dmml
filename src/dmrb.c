
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmrb.h"
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

/* TODO: Write a gzFile wrapper for stdio, and then I can just 
  use gzFile everywhere regardless if I have zlib or not */

#ifdef HAVE_ZLIB

#	include <zlib.h>

/* NOTE: Here, I'm translating the zlib I/O to stdio */

static
void *
vfopen_(const char *filename, const char *mode)
{
	assert(filename != NULL && mode != NULL);
	
	/* Open file */
	return gzopen(filename, mode);
}

static
void
vfsetbuf_(void *stream, char *buffer)
{ (void)stream, (void)buffer; }

static
void
vfclosep_(void **streamp)
{
	assert(streamp != NULL && *streamp != NULL);

	/* Close file */
	(void)gzclose_r((gzFile)*streamp);

#ifndef NDEBUG
	/* Kill handle */
	*streamp = NULL;
#endif
}

static
int
vfseek_(void *stream, long offset, int whence)
{
	z_off_t result;

	assert(stream != NULL);

	/* Seek */
	result = gzseek((gzFile)stream, (z_off_t)offset, whence);
	if (result == -1)
		return -1;

	return 0;
}

static
size_t
vfread_(void *data, size_t size, size_t count, void *stream)
{
	int result;

	assert((data != NULL || (size * count < 1)) && stream != NULL);

	/* Read */
	result = gzread((gzFile)stream, data, (unsigned)(size * count));
	if (result < 0)
		return 0;

	return (size_t)result / size;
}

static
int
vfeof_(void *stream)
{
	assert(stream != NULL);

	/* Check for EOF */
	return gzeof((gzFile)stream);
}

static
int
vferror_(void *stream)
{
	int result;

	assert(stream != NULL);

	/* Check for error */
	return gzerror((gzFile)stream, &result) != NULL;
}

#else /* !HAVE_ZLIB */

static
void *
vfopen_(const char *filename, const char *mode)
{
	assert(filename != NULL && mode != NULL);

	/* Try opening the file */
	return fopen(filename, mode);
}

static
void
vfsetbuf_(void *stream, char *buffer)
{
	assert(stream != NULL);

	/* Set buffer */
	setbuf((FILE *)stream, buffer);
}

static
void
vfclosep_(void **streamp)
{
	assert(streamp != NULL && *streamp != NULL);

	/* Close file */
	fclose((FILE *)*streamp);

#ifndef NDEBUG
	/* Zero file pointer to facilitate debugging */
	*streamp = NULL;
#endif
}

static
int
vfseek_(void *stream, long offset, int whence)
{
	assert(stream != NULL);

	/* Seek */
	return fseek((FILE *)stream, offset, whence);
}

static
size_t
vfread_(void *data, size_t size, size_t count, void *stream)
{
	assert((data != NULL || (size * count) < 1) && stream != NULL);

	/* Read data */
	return fread(data, size, count, (FILE *)stream);
}

static
int
vfeof_(void *stream)
{
	assert(stream != NULL);
	
	/* Check for EOF */
	return feof((FILE *)stream);
}

static
int
vferror_(void *stream)
{
	assert(stream != NULL);

	/* Check for errors */
	return ferror((FILE *)stream);
}

#endif /* !HAVE_ZLIB */

/* Chars (typically, but not necessarily bytes) per uint32_t;
 * the code works fine if char is 16 or 32 bits, otherwise it won't
 * compile as uint32_t is not implemented */
#define DMRB_UINT32_CHARS_ (sizeof(uint32_t))

/* Chars per record, header */
#define DMRB_RECORD_CHARS_ (4 * DMRB_UINT32_CHARS_) 
#define DMRB_HEADER_CHARS_ (2 * DMRB_RECORD_CHARS_)

/* Magic word for a 'dmrb' file */
#define DMRB_MAGIC_ ((uint32_t)0x62726d64u)

/* TODO: I am looking at the asm and these are not optimized away;
 * put these in a header with #ifdefs for Intel? */

static
uint32_t
get_u32_le_(const char *data)
{
#ifdef HAVE_INTEL
	/* On i386 or amd64, endianess is correct and we can do unaligned loads */
	return *(uint32_t *)data;

#else
	uint32_t result;
	size_t i;

	/* Loop */
	result = 0;
	for (i = sizeof(result); i-- > 0;)
		result = (result << CHAR_BIT) | (uint32_t)(unsigned char)data[i];

	return result;

#endif
}

static
uint64_t
get_u64_le_(const char *data)
{
#ifdef HAVE_INTEL
	/* On i386 or amd64, endianess is correct and we can do unaligned loads */
	return *(uint64_t *)data;

#else
	uint64_t result;
	size_t i;

	/* Loop */
	result = 0;
	for (i = sizeof(result); i-- > 0;)
		result = (result << CHAR_BIT) | (uint64_t)(unsigned char)data[i];

	return result;
#endif
}

static
void
dmrb_xlat_record_(dmrb_record_t *record, const char *data)
{
	/* Translate values */
	record->site = get_u32_le_(&data[0 * DMRB_UINT32_CHARS_]);
	record->read = get_u32_le_(&data[1 * DMRB_UINT32_CHARS_]);
	record->value = get_u64_le_(&data[2 * DMRB_UINT32_CHARS_]);
}

static
int
dmrb_check_magic_(const char *data)
{
	assert(data != NULL);

	/* Check magic */
	if (get_u32_le_(data) != DMRB_MAGIC_)
	{
		/* TODO: set a meaningful errno on POSIX etc.*/

		/* Set error */
		errno = -1;

		return -1;
	}
	return 0;
}

/* GEt mask for lowest set bit (i.e. zero high bits */
#define bitops_lobit_(x) \
	((x) & ~((x) - 1))

static
int
dmrb_read_header_(dmrb_record_t *limits, void *stream)
{
	char header[DMRB_HEADER_CHARS_];

	assert(limits != NULL && stream != NULL);

	/* Read header */
	if (vfread_(header, 1, sizeof(header), stream) != sizeof(header))
		return -1;

	/* Check magic */
	if (dmrb_check_magic_((const char *)&header[0]) != 0)
		return -1;

	/* Get limits */
	dmrb_xlat_record_(limits, (const char *)&header[DMRB_RECORD_CHARS_]);
	limits->value = bitops_lobit_(limits->value);

	return 0;
}

int
dmrb_reader_init(dmrb_reader_t *self, const char *filename)
{
	assert(self != NULL && filename != NULL);

	/* Open file */
	self->file_ = vfopen_(filename, "rb");
	if (self->file_ == NULL)
		return -1;

	/* Disable buffering */
	vfsetbuf_(self->file_, NULL);

	/* Read header */
	if (dmrb_read_header_(&self->limits_, self->file_) != 0)
	{
		/* Close file */
		vfclosep_(&self->file_);

		return -1;
	}

	return 0;
}

const dmrb_record_t *
dmrb_reader_limits(const dmrb_reader_t *self)
{
	assert(self != NULL);

	/* Return limits */
	return &self->limits_;
}

int
dmrb_reader_rewind(dmrb_reader_t *self)
{
	assert(self != NULL);

	/* Seek to just past the headers */
	if (vfseek_(self->file_, DMRB_HEADER_CHARS_, SEEK_SET) != 0)
		return -1;

	return 0;
}

int
dmrb_reader_read(dmrb_reader_t *self, dmrb_record_t *records, size_t count)
{
	size_t top, i;

	assert(self != NULL && (records != NULL || count < 1) && count <= INT_MAX);

	/* Read data */
	top = vfread_(records, DMRB_RECORD_CHARS_, count, self->file_);
	if (top == 0)
	{
		/* All good? */
		if (!vferror_(self->file_))
			return 0;

		return 0;
	}

	/* NOTE: This loop is supposed to optimized away on LE,
	 * and turned into byte swaps on BE */

	/* Translate records */
	for (i = top; i-- > 0;)
		dmrb_xlat_record_(&records[i], (char *)records + i * DMRB_RECORD_CHARS_);

	return (int)top;
}

int
dmrb_reader_eof(const dmrb_reader_t *self)
{
	assert(self != NULL);

	/* Check for EOF */
	if (vfeof_(self->file_))
		return 1;
	/* Check for I/O errors */
	if (vferror_(self->file_))
		return -1;

	return 0;
}

void
dmrb_reader_deinit(dmrb_reader_t *self)
{
	assert(self != NULL);

	/* Close file */
	vfclosep_(&self->file_);
}
