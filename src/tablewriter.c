
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "tablewriter.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

#ifdef HAVE_ZLIB
# include <zlib.h>
#endif

struct tablewriter_methods_ {
	int (*addcolumn)(tablewriter_t *, const char *);
	int (*put)(tablewriter_t *, const double *, size_t);
	int (*flush)(tablewriter_t *);
};

static
int
invalid_addcolumn(tablewriter_t *self, const char *colname)
{
	assert(self != NULL);
	assert(colname != NULL);

	/* Just report an error */
	(void)self, (void)colname;
	return -1;
}

static
int
invalid_put(tablewriter_t *self, const double *values, size_t count)
{
	assert(self != NULL);
	assert(values != NULL || count < 1);

	/* Report error */
	(void)self, (void)values, (void)count;
	return -1;
}

static
int
invalid_flush(tablewriter_t *self)
{
	assert(self != NULL);

	/* Error out */
	(void)self;
	return -1;
}

static const struct tablewriter_methods_ invalid_methods =
	{ &invalid_addcolumn, &invalid_put, &invalid_flush };

static
int
binary_addcolumn(tablewriter_t *self, const char *colname)
{
	assert(self != NULL);
	assert(colname != NULL);

	/* Do nothing */
	(void)self, (void)colname;
	return 0;
}

static
int
binary_put(tablewriter_t *self, const double *values, size_t count)
{
	assert(self != NULL);
	assert(values != NULL || count < 1);

#ifdef HAVE_ZLIB
	/*
	 * Oh zlib, unsigned-wide sizes.. seriously..?
	 */
	{ unsigned maxcount;
	maxcount = (unsigned)-1 / (unsigned)sizeof(*values);

	while (count > maxcount)
	{
		/* Write what we can.. */
		if (gzwrite((gzFile)self->file_, values, maxcount * (unsigned)sizeof(*values)) < 1)
			return -1;
		
		/* Update */
		values = &values[maxcount];
		count -= maxcount;
	}

	if (count > 0) /* NOTE: gzwrite uses 0 for error */
		if (gzwrite((gzFile)self->file_, values, (unsigned)count * (unsigned)sizeof(values)) < 1)
			return -1;

	}
#else
	/* Write */
	if (fwrite(values, sizeof(*values), count, (FILE *)self->file_) != count)
		return -1;
#endif

	return 0;
}

static
int
binary_flush(tablewriter_t *self)
{
	assert(self != NULL);

	/* Do nothing */
	(void)self;
	return 0;
}

static const struct tablewriter_methods_ binary_methods =
	{ &binary_addcolumn, &binary_put, &binary_flush };

/* Newline & delimiter for the text format file */
#define TEXT_NEWLINE "\n"
#define TEXT_DELIM "\t"

static
int
text_addcolumn(tablewriter_t *self, const char *colname)
{
	const char *sep;

	assert(self != NULL);
	assert(colname != NULL);

	/* Figure out separator (self->sep_ is for values) */
	sep = TEXT_DELIM;
	if (self->cols_ < 1)
		sep = "";

	/* TODO: Quote column names if necessary--I don't need this yet */

	/* Write column name */
#ifdef HAVE_ZLIB
	if (!(sep[0] == '\0' && colname[0] == '\0')) /* NOTE: gzprintf uses 0 for error */
		if (gzprintf((gzFile)self->file_, "%s%s", sep, colname) < 1)
			return -1;
#else
	if (fprintf((FILE *)self->file_, "%s%s", sep, colname) < 0)
		return -1;
#endif

	/* Bump column count & make sure to output a linebreak */ 
	++self->cols_;
	self->sep_ = TEXT_NEWLINE;

	return 0;
}

static
int
text_put(tablewriter_t *self, const double *values, size_t count)
{
	size_t i;

	assert(self != NULL);
	assert(values != NULL || count < 1);

	/* Loop */
	for (i = 0; i < count; ++i)
	{
		/* Write value */
#ifdef HAVE_ZLIB
		if (gzprintf((gzFile)self->file_, "%s%.15g", self->sep_, values[i]) < 1)
			return -1;
#else
		if (fprintf((FILE *)self->file_, "%s%.15g", self->sep_, values[i]) < 0)
			return -1;
#endif

		/* Update separator */
		self->sep_ = TEXT_DELIM;

		/* Track column */
		if (!(++self->col_ < self->cols_))
		{
			self->col_ = 0;
			self->sep_ = TEXT_NEWLINE;
		}
	}

	return 0;
}

static
int
text_flush(tablewriter_t *self)
{
	assert(self != NULL);

	/* Write final separator */
#ifdef HAVE_ZLIB
	if (self->sep_[0] != '\0')
		if (gzprintf((gzFile)self->file_, "%s", self->sep_) < 1) /* NOTE: 0 is error */
			return -1;
#else
	if (fprintf((FILE *)self->file_, "%s", self->sep_) < 0)
		return -1;
#endif

	return 0;
}

static const struct tablewriter_methods_ text_methods = 
	{ &text_addcolumn, &text_put, &text_flush };

int
tablewriter_init(tablewriter_t *self, const char *filename, const char *format)
{
#ifdef HAVE_ZLIB
	static const char *const modes[2][2] = { { "wT", "w" }, { "wbT", "wb" } };
#else
	static const char *const modes[2] = { "w", "wb" };
#endif

	int is_bin, is_gzip;
	size_t i;
#ifdef HAVE_ZLIB
	gzFile file;
#else
	FILE *file;
#endif

	assert(self != NULL);
	assert(filename != NULL);
	assert(format != NULL);

	/* Parse format */
	is_bin = is_gzip = 0;
	for (i = 0; format[i] != '\0'; ++i)
	{
		is_bin = is_bin || format[i] == 'b';
		is_gzip = is_gzip || format[i] == 'z';
	}

	/* Open file */
#ifdef HAVE_ZLIB
	file = gzopen(filename, modes[is_bin][is_gzip]);
#else
	(void)is_gzip;
	file = fopen(filename, modes[is_bin]);
#endif
	if (file == NULL)
	{
		/* Put in empty methods */
		self->file_ = NULL;
		self->methods_ = &invalid_methods;

		return -1;
	}
	
	/* TODO: If I don't have zlib and "z" was requested, I should write a gzip
	 * header before the uncompressed data
	 */

	/* Put in methods */
	self->file_ = file;
	self->methods_ = is_bin ? &binary_methods : &text_methods;

	/* Initialize fields for text mode */
	if (!is_bin)
	{
		self->col_ = self->cols_ = 0u;
		self->sep_ = "";
	}

	return 0;
}

int
tablewriter_addcolumn(tablewriter_t *self, const char *colname)
{
	assert(self != NULL);

	/* Call the appropriate method */
	return (*self->methods_->addcolumn)(self, colname);
}

int
tablewriter_put(tablewriter_t *self, const double *values, size_t count)
{
	assert(self != NULL);
	assert(values != NULL || count < 1);

	/* Call the method */
	return (*self->methods_->put)(self, values, count);
}

int
tablewriter_deinit(tablewriter_t *self)
{
	int error;

	assert(self != NULL);

	/* "Flush" */
	error = (*self->methods_->flush)(self);

	/* Stop if file is already stale */
	if (self->file_ == NULL)
		return -1;

#ifdef HAVE_ZLIB
	/* Add read errors */
	if (error != 0)
		(void)gzerror((gzFile)self->file_, &error);

	/* Close file */
	if (gzclose_w((gzFile)self->file_) != 0)
		error = -1;
#else
	/* Get pending errors */
	if (error != 0)
		error = ferror((FILE *)self->file_);

	/* Close */
	if (fclose((FILE *)self->file_) != 0)
		error = -1;
#endif

	/* Check for any error */
	if (error != 0)
		return error;

	return 0;
}
