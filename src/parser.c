
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmml/parser.h"
#include "imath.h"
#include <assert.h>
#include <stddef.h>

void
dmml_parser_init(dmml_parser_t *parser, uint8_t letters, uint8_t order)
{
	assert(parser != NULL);
	assert(letters > 0u);
	assert(order > 0u);

	/* Copy in the dimensions */
	parser->letters_ = letters;
	parser->order_ = order;

	/* TODO: check that ipowl(parser->base_, order) - 1
	 does not overflow */

	/* Compute modulo & shift */
	parser->base_ = letters + 1u;
	parser->shift_ = ipowl(parser->base_, order - 1u);

	/* Zero position & current combination */
	parser->site_ = 0u;
	parser->comb_ = 0u;
}

uint32_t
dmml_parser_tell(const dmml_parser_t *parser)
{
	assert(parser != NULL);

	/* Get current site */
	return parser->site_;
}

void
dmml_parser_seek(dmml_parser_t *parser, uint32_t site)
{
	assert(parser != NULL);

	/* Set current site & reset combination */
	parser->site_ = site;
	parser->comb_ = 0u;
}

uint32_t
dmml_parser_skip(dmml_parser_t *parser, uint32_t count)
{
	assert(parser != NULL);

	/* Early exit on zero count */
	if (count < 1u)
		return parser->comb_;

	/* Short skip? */
	if (count < parser->order_)
		/* Shift out digits */
		parser->comb_ /= ipowl(parser->base_, count);
	else
		/* We're all NAs now */
		parser->comb_ = 0u;

	/* Slide window */
	parser->site_ += count;

	return parser->comb_;
}

uint32_t
dmml_parser_put(dmml_parser_t *parser, uint8_t letter)
{
	assert(parser != NULL);
	assert(letter < parser->letters_);

	/* Shift out a digit & push in the next one */
	parser->comb_ /= parser->base_;
	parser->comb_ += parser->shift_ * (letter + 1);

	/* Slide window */
	++parser->site_;

	return parser->comb_;
}

static
void
counts_tag(uint32_t *counts, uint32_t site_offset, uint32_t sites, const dmml_parser_t *parser, uint32_t site, uint32_t comb)
{
	size_t combs, slots;

	/* NOTE: move to size_t here, we will need the extra precision for the
	 * subscripts to counts */

	/* TODO: the overflow check for this should be somewhere, but not here */

	/* Compute dimensions */
	combs = (size_t)parser->shift_ * parser->base_;
	slots = (size_t)sites + (parser->order_ - 1u);

	/* If the site is within bounds, tag it */
	if (site_offset <= site && site < site_offset + slots)
		++counts[(site - site_offset) * combs + comb];
}

static
void
counts_flush(uint32_t *counts, uint32_t site_offset, uint32_t sites, dmml_parser_t *parser, uint32_t skip)
{
	uint32_t site, comb;

	/* Loop */
	while (skip-- > 0u)
	{
		/* Get site & rotate window */
		site = dmml_parser_tell(parser);
		comb = dmml_parser_skip(parser, 1u);

		/* Stop if the window has been emptied? */
		if (!(comb > 0u))
		{
			/* Fast forward to position */
			dmml_parser_skip(parser, skip);
			break;
		}

		/* If not, tag */
		counts_tag(counts, site_offset, sites, parser, site, comb);
	}
}

int
dmml_parser_push(dmml_parser_t *parser, uint32_t *counts, uint32_t site_offset, uint32_t sites, int read_edge, uint32_t site, uint8_t letter)
{
	uint32_t comb;

	/* Should we change the read? */
	if (!read_edge)
	{
		/* Check site */
		if (!(site >= parser->site_))
			return -1;

		/* Skip over gaps */
		counts_flush(counts, site_offset, sites, parser, site - parser->site_);
	}
	else
	{
		/* Flush */
		counts_flush(counts, site_offset, sites, parser, parser->order_);

		/* Jump */
		dmml_parser_seek(parser, site);
	}

	/* Push & record sample */
	comb = dmml_parser_put(parser, letter);
	counts_tag(counts, site_offset, sites, parser, site, comb);

	return 0;
}

void
dmml_parser_flush(dmml_parser_t *parser, uint32_t *counts, uint32_t site_offset, uint32_t sites)
{
	/* Flush window */
	counts_flush(counts, site_offset, sites, parser, parser->order_);
}
