
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmml/halton.h"
#include <assert.h>
#include <math.h>
#include <string.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

double
dmml_haltonnumber(unsigned long index, unsigned long base)
{
	double result, f;
	unsigned long i;

	assert(base > 0);

	/* Set up */
	result = 0.;
	f = 1.;
	i = index + 1;

	/* Decompose each digit */
	while (i > 0)
	{
		f /= base;
		result += f * (i % base);
		i /= base;
	}

	return result;
}

static
unsigned long
prime(unsigned long index)
{
	static const unsigned primes[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31,
		37, 41 , 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101 , 103, 107,
		109, 113, 127, 131 };

	/* TODO: Larger numbers--for now this is ok, we can't have more than 32
	  dimensions anyway */

	if (!(index < countof(primes)))
		return (unsigned long)-1;

	return primes[index];
}

static
double
ffloormod(double x, double y)
{
	double r;

	/* Get remainder & rewrap */
	r = fmod(x, y);
	if (r < 0.)
		r += y;

	return r;
}

double
dmml_halton(unsigned long index, unsigned long dim, double seed)
{
	unsigned long base;
	double shift;

	/* Find base & shift for the dimension */
	base = prime(dim);
	shift = seed - dmml_haltonnumber(0, base);

	/* Get the number */
	return ffloormod(shift + dmml_haltonnumber(index, base), 1.);
}
