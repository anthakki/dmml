
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "dmrb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
char *
chomp(char *line)
{
	size_t tail;

	/* Find line length */
	tail = strlen(line);

	/* Strip newline if present */
	if (tail > 0 && line[tail-1] == '\r')
		--tail;
	if (tail > 0 && line[tail-1] == '\n')
		--tail;

	/* Terminate */
	line[tail] = '\0';
	return line;
}

static
int
dmra_fetch_row(dmrb_reader_t *self, unsigned long *row)
{
	char line[64];
	int tail;

	/* Read line */
	if (fgets(line, countof(line), (FILE *)self->file_) == NULL)
		return feof((FILE *)self->file_) ? 0 : -1;
	if (line[0] == '\0')
		return 0;

	/* TODO: value/cookie might overflow long */
	/* TODO: I should validate range, sscanf() won't */

	/* Parse */
	if (sscanf(line, "%lu" "\t" "%lu" "\t" "%lu" "\t" "%lu" "%n",
			&row[0], &row[1], &row[2], &row[3], &tail) < 4)
		return -1;
	if (!((size_t)tail < countof(line)-1 && *chomp(&line[tail]) == '\0'))
		return -1;

	/* Adjust indices */
	row[0] = row[0] - 1;
	row[1] = row[1] - 1;

	return 1;
}

static
int
dmra_reader_init(dmrb_reader_t *self, const char *filename)
{
	static const char header[] =
		"#site" "\t" "read" "\t" "value" "\t" "cookie";

	char buffer[32];
	fpos_t start;
	unsigned long row[4];

	/* Open file */
	self->file_ = fopen(filename, "r");
	if (self->file_ == NULL)
		return -1;

	/* Check header line */
	if (fgets(buffer, countof(buffer), (FILE *)self->file_) == NULL)
		return -1;
	if (strcmp(header, chomp(buffer)) != 0)
		return -1;

	/* Store position */
	if (fgetpos((FILE *)self->file_, &start) != 0)
		return -1;

	/* Find limits */
	memset(&self->limits_, 0, sizeof(self->limits_));
	while (dmra_fetch_row(self, row) > 0)
	{
		if (row[0] >= self->limits_.site)
			self->limits_.site = row[0] + 1;
		if (row[1] >= self->limits_.read)
			self->limits_.read = row[1] + 1;
		if (row[2] >= self->limits_.value)
			self->limits_.value = row[2] + 1;
	}

	/* Check for errors */
	if (!feof((FILE *)self->file_))
		return -1;

	/* Rewind file */
	if (fsetpos((FILE *)self->file_, &start) != 0)
		return -1;

	return 0;
}

static
int
dmra_reader_read(dmrb_reader_t *self, dmrb_record_t *record, size_t count)
{
	unsigned long row[4];
	size_t top;
	int result;

	/* Loop & read data */
	for (top = 0; top < count; ++top)
	{
		/* Read row */
		result = dmra_fetch_row(self, row);
		if (result < 0)
			return -1;
		else if (!(result > 0))
			break;

		/* Convert */
		record->site = (uint32_t)row[0];
		record->read = (uint32_t)row[1];
		record->value = (uint64_t)row[3] * self->limits_.value + (uint64_t)row[2];
	}

	return top;
}

static
void
dmra_reader_deinit(dmrb_reader_t *self)
{ fclose((FILE *)self->file_); }

static
int
dmra_write_record(FILE *stream, const dmrb_record_t *limits, const dmrb_record_t *record)
{
	/* TODO: long might not be wide enough for value/cookie */

	/* Write record */
	(void)fprintf(stream, "%lu" "\t" "%lu" "\t" "%lu" "\t" "%lu" "\n",
		(unsigned long)(record->site + 1), (unsigned long)(record->read + 1),
			(unsigned long)(record->value % limits->value),
				(unsigned long)(record->value / limits->value));

	return 0;
}

static
int
dmra_write_header(FILE *stream, const dmrb_record_t *limits)
{
	/* Write header row */
	(void)limits;
	(void)fprintf(stream, "#" "site" "\t" "read" "\t" "value" "\t" "cookie" "\n");

	return 0;
}

static
int
dmrb_write_record(FILE *stream, const dmrb_record_t *limits, const dmrb_record_t *record)
{
	/* Write record */
	(void)limits;
	(void)fwrite(&record->site, sizeof(record->site), 1, stream);
	(void)fwrite(&record->read, sizeof(record->read), 1, stream);
	(void)fwrite(&record->value, sizeof(record->value), 1, stream);

	return 0;
}

static
int
dmrb_write_header(FILE *stream, const dmrb_record_t *limits)
{
	static const dmrb_record_t magic = { 0x62726d64, 0, 0 }; /* 'dmrb', 0, 0 */

	/* TODO: Something like this for Windows to force stdout
	  in binary mode:
#ifdef _WIN32
	setmode(fileno(stream), O_BINARY);
#endif
	*/

	/* Write magic & limits */
	return dmrb_write_record(stream, limits, &magic) == 0 && 
		dmrb_write_record(stream, limits, limits) == 0 ? 0 : -1;
}

struct view_format {
	int (*write_header)(FILE *, const dmrb_record_t *);
	int (*write_record)(FILE *, const dmrb_record_t *, const dmrb_record_t *);
};

static const struct view_format view_format_ascii = { &dmra_write_header, &dmra_write_record };
static const struct view_format view_format_binary = { &dmrb_write_header, &dmrb_write_record };

struct options {
	/* Method to invoke */
	const char *(*method)(const struct options *, const char *); 
	/* Format for dumping data for the "view" method */
	const struct view_format *view_format;
};

static
const char *
dmrview(const struct options *opts, const char *filename)
{
	dmrb_reader_t reader;
	int (*read)(dmrb_reader_t *, dmrb_record_t *, size_t);
	const dmrb_record_t *limits;
	dmrb_record_t last_record, record;

	/* Open file */
	if (dmrb_reader_init(&reader, filename) == 0)
		read = &dmrb_reader_read;
	else if (dmra_reader_init(&reader, filename) == 0)
		read = &dmra_reader_read;
	else
		return "failed to open for read";

	/* Write header */
	limits = dmrb_reader_limits(&reader);
	(*opts->view_format->write_header)(stdout, limits);

	/* Set up (hopefully) invalid record */
	last_record.site = last_record.read = (uint32_t)-1;

	/* Loop */
	for (; (*read)(&reader, &record, 1) > 0; last_record = record)
	{
		/* Write row */
		(*opts->view_format->write_record)(stdout, limits, &record);

		/* Data not sorted? */
		if (record.read == last_record.read && !(record.site > last_record.site))
		{
			/* Print a warning */
			fprintf(stderr, "warning: the following record is not sorted within the read:" "\n" ">\t");
			dmra_write_record(stderr, limits, &record);
		}
	}

	/* Check for errors */
	if (read == &dmrb_reader_read)
	{
		if (!(dmrb_reader_eof(&reader) > 0))
		{
			/* Close & error out */
			dmrb_reader_deinit(&reader);
			return "read error";
		}
	
		/* Close */
		dmrb_reader_deinit(&reader);
	}
	else /* if (read == &dmra_reader_read) */
	{
		if (!(dmra_reader_read(&reader, &record, 1) >= 0))
		{
			/* Close & error out */
			dmra_reader_deinit(&reader);
			return "read error";
		}

		/* Close */
		dmra_reader_deinit(&reader);
	}

	return NULL;
}

static
void
setdefaults(struct options *opts)
{
	/* Default options */
	opts->method = &dmrview;
	opts->view_format = &view_format_ascii;
}

static
int
parseargs(struct options *opts, int argc, char **argv)
{
	/* Check syntax */
	switch (argc)
	{
		case 4:
			/* Check that we have the -b switch */
			if (strcmp(argv[2], "-b") != 0)
				goto invalid_args;

			/* Set binary flag */
			opts->view_format = &view_format_binary;

			/* FALLTHROUGH */

		case 3:
			/* Check for the "view" verb */
			if (strcmp(argv[1], "view") != 0)
				goto invalid_args;

			/* Good */
			break;

		default:
			/* FALLTHROUGH */

		invalid_args:
			fprintf(stderr, "%s: invalid arguments" "\n", argv[0]);
			return -1;
	}

	/* ASsume last argument is the filename */
	return argc - 1;
}

static
void
usage(const char *argv0)
{
	fprintf(stderr,
"Usage: %s view [-b] file.dmrb" "\n"
		, argv0);
}

int
main(int argc, char **argv)
{
	int optind;
	struct options opts;
	const char *errmsg;

	/* Set defaults */
	setdefaults(&opts);

	/* Parse arguments */
	optind = parseargs(&opts, argc, argv);
	if (optind < 0)
	{
		/* Show usage */
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	/* Invoke */
	errmsg = (*opts.method)(&opts, argv[optind]);
	if (errmsg != NULL)
	{
		fprintf(stderr, "%s: %s: %s" "\n", argv[0], argv[optind], errmsg);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
