
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include <stdio.h>
#include <stdlib.h>

struct options {
	const char *fieldnames;
	size_t width;
};

static
void
setdefaults(struct options *opts)
{
	/* No field name */ 
	opts->fieldnames = NULL;
	/* Width of 1 */
	opts->width = 0;
}

static
int
parseargs(struct options *opts, int argc, char **argv)
{
	int ind, d1;
	char stop;
	unsigned long lu1;

	/* Parse options */
	for (ind = 1; ind < argc && argv[ind][0] == '-'; ++ind)
		if (argv[ind][1] == '-')
			switch (argv[ind][2])
			{
				case 'f':
					if (sscanf(argv[ind], "--fieldnames=%n %c", &d1, &stop) == 1)
					{
						opts->fieldnames = &argv[ind][d1];
						break;
					}
					goto invalid_opt;

				case 'w':
					if (sscanf(argv[ind], "--width=%lu %c", &lu1, &stop) == 1)
					{
						opts->width = (size_t)lu1;
						break;
					}
					goto invalid_opt;

				default:
					/* FALLTHROUGH */

				invalid_opt:
					fprintf(stderr, "%s: invalid option %s" "\n", argv[0], argv[ind]);
					return -1;
			}
		else
			goto invalid_opt;

	return ind;
}

static
void
usage(const char *argv0)
{
	fprintf(stderr,
"Usage: %s [--fieldnames=f1[,f2,[...]]] [--width=w] [input.pd [...]]" "\n"
		, argv0);
}

#define FIELDNAMES_DELIM ','
#define FIELDNAMES_QUOTE '"'

static
size_t
detectwidth(const char *fieldnames)
{
	size_t fields, i;
	int inquote;

	/* Set up defaults */
	fields = 1;
	inquote = 0;

	/* Count separators in field names */
	for (i = 0; fieldnames[i] != '\0'; ++i)
		if (fieldnames[i] == FIELDNAMES_DELIM)
		{
			if (!inquote)
				++fields;
		}
		else if (fieldnames[i] == FIELDNAMES_QUOTE)
			inquote = !inquote;

	return fields;
} 

#define PRINT_NEWLINE_S "\n"
#define PRINT_DELIM   '\t'

static
void
printheader(FILE *dest, const struct options *opts)
{
	int inquote;
	size_t i;

	/* Stop if no field names */
	if (opts->fieldnames == NULL)
		return;

	/* Set up state */
	inquote = 0;

	/* Count separators in field names */
	for (i = 0; opts->fieldnames[i] != '\0'; ++i)
	{
		/* Select output */
		switch (opts->fieldnames[i])
		{
			case PRINT_DELIM:
				if (!inquote)
				{
					/* Quote delimiter */
					fputc(FIELDNAMES_QUOTE, dest); 
					fputc(PRINT_DELIM, dest);
					fputc(FIELDNAMES_QUOTE, dest);
					break;
				}
				else
					goto change_delim;

			case FIELDNAMES_DELIM:
				if (!inquote)
				{
change_delim:
					/* Convert delimiter */
					fputc(PRINT_DELIM, dest);
					break;
				}
				/* FALLTHROUGH */

			default:
				fputc(opts->fieldnames[i], dest);
				break;
		}

		/* Update state */
		if (opts->fieldnames[i] == FIELDNAMES_QUOTE)
			inquote = !inquote;
	}

	/* Write newline */
	fprintf(dest, PRINT_NEWLINE_S);
}

static
const char *
printfile(FILE *dest, FILE *src, const struct options *opts)
{
	double value;
	size_t i;

	/* Read in first field */
	while (fread(&value, sizeof(value), 1, src) == 1)
	{
		/* Print */
		fprintf(dest, "%.15g", value);

		/* Read in the rest */
		for (i = 1; i < opts->width; ++i)
		{
			/* Read */
			if (fread(&value, sizeof(value), 1, src) != 1)
			{
				/* Print missing newline */
				fprintf(dest, PRINT_NEWLINE_S);

				/* Return error */
				return "number of items is not divisible by width";
			}
			/* Print */
			fputc(PRINT_DELIM, dest);
			fprintf(dest, "%.15g", value);
		}

		/* Write newline */
		fprintf(dest, PRINT_NEWLINE_S);

	}

	/* Read error */
	if (!feof(src))
		return "read error";

	return NULL;
}

int
main(int argc, char **argv)
{
	struct options opts;
	int optind, ind;
	const char *filename, *errmsg;

	/* Set defaults */
	setdefaults(&opts);

	/* Parse arguments */
	optind = parseargs(&opts, argc, argv);
	if (optind < 0)
	{
		/* Show usage */
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	/* Auto-detect width */
	if (opts.width < 1 && opts.fieldnames != NULL)
		opts.width = detectwidth(opts.fieldnames);

	/* Print header */
	printheader(stdout, &opts);

	/* Process file(s) */
	if (optind < argc)
		for (ind = optind; ind < argc; ++ind)
		{
			FILE *file;

			/* Get file name */
			filename = argv[ind];

			/* Open file */
			file = fopen(filename, "rb");
			if (file == NULL)
			{
				/* Print error */
				errmsg = "failed to open for read";
				goto print_errmsg;
			}

			/* Process */
			errmsg = printfile(stdout, file, &opts);

			/* Close file */
			fclose(file);

			/* Error? */
			if (errmsg != NULL)
			{
print_errmsg:
				/* Print error */
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], filename, errmsg);
				return EXIT_FAILURE;
			}
		}
	else
	{
		/* Process */
		errmsg = printfile(stdout, stdin, &opts);

		/* Error? */
		if (errmsg != NULL)
		{
			/* Print error */
			filename = "(stdin)";
			goto print_errmsg;
		}
	}

	return EXIT_SUCCESS;
}
